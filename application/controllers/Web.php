<?php

header('Access-Control-Allow-Origin: *');  

class Web extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('global_helper');
        //load model admin
        $this->load->model('web_model');
    }


    public function index(){
    	$data['body'] = true;
        $foot['menu'] = true;
        $foot['active'] = 'home';
    	$this->load->view('web/header',$data);
    	$this->load->view('web/home');
    	$this->load->view('web/footer', $foot);
    }

     public function checkout(){
        if($this->session->userdata('id_user')){
            if($this->web_model->cek_order() > 0){
                $tgl_pengiriman = $this->web_model->hari_pengiriman();
              

                $provinsi = $this->db->get_where('provinsi', array('flag' => 1))->result_array();
                $data['provinsi'] = $provinsi;
                $data['cart'] = true;
                $data['tgl_pengiriman'] = $tgl_pengiriman;

                $this->load->view('web/header',$data);
                $this->load->view('web/checkout',$data);
                $this->load->view('web/footer');  
            }else{
                redirect(base_url());
            }
            
        }else{
            redirect(base_url());
        }

        
    }

    public function notifikasi(){
        if($this->session->userdata('id_user')){

                $this->load->view('web/header');
                $this->load->view('web/notifikasi');
                $this->load->view('web/footer');          
            
        }else{
            redirect(base_url());
        }

        
    }
    public function privacy(){
        $this->load->view('web/privacy');
    }

    public function pembayaran(){
        


        $id_user = $this->session->userdata('id_user');
        $id_transaksi = $this->uri->segment(3);
        $payment = $this->db->query('SELECT a.*,  DATE(tgl_order) as tgl_aja,  b.nama_bank, b.no_rekening,b.logo from transaksi_temp a 
            LEFT JOIN data_bank b ON a.id_bank_pembayaran = b.id 
            WHERE a.id_user="'.$id_user.'"  AND a.id="'.$id_transaksi.'" ');

        if($payment->row_array() > 0){
            $payment = $payment->row_array();

             $data['payment'] = $payment;
                $data['cart'] = true;
                $this->load->view('web/header',$data);
                $this->load->view('web/pembayaran');
                $this->load->view('web/footer');
        }else{
            redirect(base_url());
        }

               
          
     
        
    }

     public function history(){
        if($this->session->userdata('id_user')){
         	$data['body'] = true;
            $foot['menu'] = true;
            $foot['active'] = 'history';
        	$this->load->view('web/header',$data);
        	$this->load->view('web/history');
        	$this->load->view('web/footer',$foot);
        }else{
            redirect(base_url());
        }
    }

     public function account(){
        if($this->session->userdata('id_user')){
         	$data['body'] = true;
            $foot['menu'] = true;
            $foot['active'] = 'account';
        	$this->load->view('web/header',$data);
        	$this->load->view('web/my_account');
        	$this->load->view('web/footer',$foot);
        }else{
            redirect(base_url());
        }
    }

     public function cart(){
        if($this->session->userdata('id_user')){
            $data['body'] = true;
            $data['cart'] = true;
            $this->load->view('web/header',$data);
            $this->load->view('web/cart_mobile');
            $this->load->view('web/footer');
        }else{
            redirect(base_url());
        }
    }

    public function load_detail_history(){
        $this->load->view('web/load_detail_history');
    }

    public function get_kota(){

        if($post = $this->input->post()){
            $kota = $this->db->get_where('kabupaten', array('id_prov' => $post['id_prov'], 'flag' => 1))->result_array();
                echo "<option>-- Pilih Kota --</option>";
            foreach ($kota as $row) {
                echo "<option value='".$row['id_kab']."'>".$row['nama']."</option>";
            }
        }
    }

    public function get_kecamatan(){

        if($post = $this->input->post()){
            $kota = $this->db->get_where('kecamatan', array('id_kab' => $post['id_kota']))->result_array();
            echo "<option>-- Pilih Kecamatan --</option>";
            foreach ($kota as $row) {
                echo "<option value='".$row['id_kec']."'>".$row['nama']."</option>";
            }
        }
    }

    public function get_kelurahan(){

        if($post = $this->input->post()){
            $kota = $this->db->get_where('kelurahan', array('id_kec' => $post['id_kec']))->result_array();
            echo "<option>-- Pilih Kelurahan --</option>";
            foreach ($kota as $row) {
                echo "<option value='".$row['id_kel']."'>".$row['nama']."</option>";
            }
        }
    }

    public function update_profile(){
        if($post = $this->input->post()){
            $id_user = $this->session->userdata('id_user');
            $data['nama'] = $post['nama'];
            $data['tgl_lahir'] = $post['tgl_lahir'];
            $data['telephone'] = $post['telephone'];
            $data['jenis_kelamin'] = $post['jenis_kelamin'];

            if($this->db->update('users', $data, array('id' => $id_user))){

            }  
        }
        
    }

    public function update_pass(){
        if($post = $this->input->post()){
            $id_user = $this->session->userdata('id_user');
            $row = $this->db->get_where('users', array('id' => $id_user))->row_array();

             if (password_verify($post['password_lama'], $row['password'])) {
                $data['password'] = password_hash($post['password_baru'], PASSWORD_BCRYPT);

                if($this->db->update('users', $data, array('id' => $id_user))){
                    echo 1;
                }
            }else{
                echo 'Password lama salah';
            }
        }
    }

    public function count_cart(){
        $id_transaksi = $this->session->userdata('kode_transaksi');

        if($id_produk = $this->input->post('id_produk')){
            $total = $this->db->query("SELECT SUM(qty) as total , SUM(a.sub_total) as grand_total FROM transaksi_temp_detail a LEFT JOIN transaksi_temp b ON a.id_transaksi = b.id WHERE b.id = '".$id_transaksi."' AND a.id_produk='".$id_produk."'")->row_array();

            $data['qty'] = $total['total'];
            $data['total'] = rupiah($total['grand_total']);
            echo json_encode($data); 

        }else{
             $total = $this->db->query("SELECT SUM(qty) as total , SUM(a.sub_total) as grand_total FROM transaksi_temp_detail a LEFT JOIN transaksi_temp b ON a.id_transaksi = b.id WHERE b.id = '".$id_transaksi."'")->row_array();
          
            $data['qty'] = $total['total'];
            $data['total'] = rupiah($total['grand_total']);
            echo json_encode($data);
        }

       
    }

    public function delete_session_transaksi(){

        if($this->session->unset_userdata('kode_transaksi')){
            return true;
        }
    }

    public function show_session(){
        echo json_encode($this->session->userdata());
       
        
    }

    public function get_cart_total_awal(){
        $id_transaksi = $this->session->userdata('kode_transaksi');

        $get = $this->db->get_where('transaksi_temp_detail', array('id_transaksi' => $id_transaksi))->result_array();

         echo json_encode($get);
    }

       
    public function minus_tr_temp(){

        $id_produk = $this->input->post('id_produk');
        $id_transaksi = $this->session->userdata('kode_transaksi');
        $get = $this->db->get_where('transaksi_temp_detail', array('id_produk' => $id_produk, 'id_transaksi' => $id_transaksi));

        if($get->num_rows() > 0){
            $qty = $get->row_array();

            if($qty['qty'] > 1){
                $upp['qty'] = $qty['qty'] - 1;
                $upp['sub_total'] = ($qty['qty'] - 1) * $qty['harga'];
                $this->db->update('transaksi_temp_detail',$upp, array('id_transaksi' => $id_transaksi,'id_produk' => $id_produk));
            }else{
                $this->db->delete('transaksi_temp_detail', array('id_transaksi' => $id_transaksi,'id_produk' => $id_produk));
            }

            $this->load->view('web/load_keranjang');
        }

        
    }


    public function add_tr_temp(){
    
        if($post = $this->input->post()){

            if($kode_transaksi = $this->session->userdata('kode_transaksi')){
                $kode_transaksi = $this->session->userdata('kode_transaksi');
                // $cek_tr = $this->db->get_where('transaksi_temp',array('id' => $kode_transaksi));


                   $id_transaksi = $kode_transaksi;

                   
                
            }else{
                
                $add['id_cabang'] = $this->session->userdata('id_cabang');
                $add['id_pengiriman'] = 0;

                $this->db->insert('transaksi_temp',$add);
                $id_transaksi = $this->db->insert_id();
           
                $session['kode_transaksi'] = $id_transaksi; 
                $this->session->set_userdata($session);
                
              

            }


            
            $produk = $this->db->query('SELECT If(promo=1,harga_promo,harga_normal) as harga FROM produk WHERE id = "'.$post['id_produk'].'" ')->row_array();

            $cek = $this->db->get_where('transaksi_temp_detail', array('id_produk' => $post['id_produk'], 'id_transaksi' => $id_transaksi));

            if($cek->num_rows() > 0){
                $cek = $cek->row_array();
                $data['sub_total'] = ($cek['qty'] + $post['qty']) * (int)$produk['harga'];
                $data['qty'] = $cek['qty'] + $post['qty'];

                if($this->db->update('transaksi_temp_detail', $data , array('id_produk' => $post['id_produk'], 'id_transaksi' => $id_transaksi))){
                    
                     $this->load->view('web/load_keranjang');
                }
            }else{
                $data['id_transaksi'] = $id_transaksi;
                $data['id_produk'] = $post['id_produk'];
                $data['harga'] = $produk['harga'];
                $data['qty'] = $post['qty'];
                $data['sub_total'] = (int)$produk['harga'] * (int)$post['qty'];
             

                if($this->db->insert('transaksi_temp_detail', $data)){
                   
                    $this->load->view('web/load_keranjang');
                } 
            }


           
        }
    }


    public function delete_tr_temp(){
        $id = $this->input->post('id');
        $kode_transaksi = $this->session->userdata('kode_transaksi');
        //$id= 4;

        if($this->db->delete('transaksi_temp_detail', array('id' => $id))){
            $id_user = $this->session->userdata('id_user');
            $cek_genrate_id = $this->db->query('SELECT * from transaksi_temp WHERE  id="'.$kode_transaksi.'" ');
            $cek_genrate_id = $cek_genrate_id->row_array();
            $id_transaksi = $cek_genrate_id['id'];

            $cek = $this->db->get_where('transaksi_temp_detail', array( 'id_transaksi' => $id_transaksi));

            // if($cek->num_rows() == 0){
            //     $this->db->delete('transaksi_temp', array('id' => $id_transaksi));
            // }

             $this->load->view('web/load_keranjang');
        }
    }

    public function add_checkout(){

        $post = $this->input->post();
        $kode_transaksi = $this->session->userdata('kode_transaksi');
        $id_user = $this->session->userdata('id_user');
        $transaksi_temp = $this->db->query('SELECT * from transaksi_temp WHERE  status != 2  AND id="'.$kode_transaksi.'" ');
        $transaksi_temp = $transaksi_temp->row_array();
        $id_transaksi = $kode_transaksi;
     
        
        $i = 0;
        $grand_total= 0;
       foreach($post['id_produk'] as $row){

        $grand_total += $post['quantity'][$i] * $post['harga'][$i];

           $data['qty'] =       $post['quantity'][$i];
           $data['id_produk'] = $post['id_produk'][$i];
           $data['sub_total'] = $post['quantity'][$i] * $post['harga'][$i];
           $data['id_transaksi'] = $id_transaksi;
           $data['id_user'] = $id_user;

           if($this->db->update('transaksi_temp_detail', $data, array('id_produk' => $post['id_produk'][$i], 'id_transaksi' => $id_transaksi))){
               
           }

            $i++;
        }



        //tambah data di tabbel transaksi temp atau update data di transaksi temp
        $cek = $this->db->get_where('transaksi_temp', array('id' => $id_transaksi));

        if($cek->num_rows() == 0){
            $add['grand_total'] = $grand_total;
            $add['id_user'] = $id_user;
            $add['status'] = 1;
             $data['id_user'] = $id_user;

            $this->db->insert('transaksi_temp', $add);
        }else{
             $add['grand_total'] = $grand_total;
             $add['id_user'] = $id_user;

            $this->db->update('transaksi_temp', $add, array('id' => $id_transaksi));
        }

    
      
    }



    public function get_alamat(){
        $this->load->view('web/load_alamat');
    }

    public function alamat_terpilih(){
        $this->load->view('web/load_pilih_pengiriman');
    }

    public function update_pegiriman(){
        if($post = $this->input->post()){
            $id_user = $this->session->userdata('id_user');
            $id_transaksi = $post['id_transaksi'];
            $data['id_pengiriman'] = $post['id_pengiriman'];

            if($this->db->update('transaksi_temp', $data, array('id_user' => $id_user, 'id' => $id_transaksi))){

            }
        }
    }

    public function update_pegiriman_tgl(){
          if($post = $this->input->post()){
            $id_user = $this->session->userdata('id_user');
            $id_transaksi = $post['id_transaksi'];
            $data['tgl_pengiriman'] = $post['tgl'];

            if($this->db->update('transaksi_temp', $data, array('id_user' => $id_user, 'id' => $id_transaksi))){

            }
        }
    }

    public function update_sess_cabang(){
        $id_cabang = $this->input->post('id_cabang');

        $get = $this->db->get_where('cabang', array('id' => $id_cabang))->row_array();
        $newdata['id_cabang'] = $id_cabang;
        $newdata['nama_cabang'] = $get['kota'];
        $this->session->set_userdata($newdata);
    }

    public function batal_pesanan_temp(){
        $id_user = $this->session->userdata('id_user');
        if($post = $this->input->post()){
            $id_transaksi = $post['id_transaksi'];
            $status = $post['status'];

            //transaksi temp
            if($status == '0'){
                $data['status'] = 0;
                $data['id_batal'] = $post['id_batal'];

                if($this->db->update('transaksi_temp', $data ,array('id' => $id_transaksi, 'id_user' => $id_user))){

                }
            }

            //transaksi
            if($status == '1'){
                  $data['status'] = 0;
                $data['id_batal'] = $post['id_batal'];

                if($this->db->update('transaksi', $data ,array('id' => $id_transaksi, 'id_user' => $id_user))){

                }
            }
        }
    }

    public function update_pembayaran(){
         if($post = $this->input->post()){
            $id_transaksi = $post['id_transaksi'];
            $id_user = $this->session->userdata('id_user');
             $kode_transaksi = 'TR/GC/'.date('Y/m/d/').$id_transaksi;


            //cek transaksi temp
            $row = $this->db->get_where('transaksi_temp', array('id' => $id_transaksi, 'id_user' => $id_user))->row_array();
             
             if($row['grand_total'] >= 100000 && $row['grand_total'] < 150000){
                $ongkir = 15000;
            }elseif($row['grand_total'] >= 150000){
                $ongkir = 0;
            }else{
                $ongkir = 20000;
            }


            $random_digit = digit_random();
            

            $id_user = $this->session->userdata('id_user');
            $data['id_bank_pembayaran'] = $post['id_bank'];
            $data['kode_transaksi'] = $kode_transaksi;
            $data['ongkir'] = $ongkir;
            $data['id_cabang'] = $this->session->userdata('id_cabang');
            $data['tgl_order'] = date('Y-m-d h:i:s');
            $data['pembayaran'] = $row['grand_total'] + $ongkir;
            $data['kode_unik'] = $random_digit;
            $data['status'] = 1;
            $data['id_cabang'] = $post['id_cabang'];

            if($this->db->update('transaksi_temp', $data, array('id' => $id_transaksi, 'id_user' => $id_user))){
                $this->delete_session_transaksi();
            }

           
        }
    }

   

    public function move_transaksi(){
            $id_user = $this->session->userdata('id_user');
            $id_transaksi = $this->input->post('id_transaksi');
      
   
            $data['status'] = 2;            
            if($this->db->update('transaksi_temp',$data, array('id' => $id_transaksi, 'id_user' => $id_user))){
                
            }
     }

    // public function move_transaksi_old(){
    //       $id_user = $this->session->userdata('id_user');
    //       $code = $this->db->query('SELECT  kode_transaksi from transaksi ORDER BY id DESC LIMIT 1');
    //         if($code->num_rows() > 0){
    //             $code = $code->row_array();
    //             $kodemax = $code['kode_transaksi'];
    //             $noUrut = substr($kodemax, 17, 4);
    //             $noUrut++;
    //             $kode_nota = sprintf("%04s", $noUrut);

    //             $kode_nota = 'TR/GC/'.date('Y/m/d/').$kode_nota;
                

    //         }else{
    //            $kode_nota = 'TR/GC/'.date('Y/m/d/').'0001'; 
              
    //         }

            
    //         $row = $this->db->get_where('transaksi_temp', array('id_user' => $id_user))->row_array();
    //         $detail_order = $this->db->get_where('transaksi_temp_detail', array('id_user' => $id_user))->result_array();

    //         if($row['grand_total'] >= 100000){
    //             $ongkir = 0;
    //         }else{
    //             $ongkir = 20000;
    //         }


    //         $data['kode_transaksi'] = $kode_nota;
    //         $data['grand_total'] = $row['grand_total'];
    //         $data['id_bank_pembayaran'] = $row['id_bank_pembayaran'];
    //         $data['id_pengiriman'] = $row['id_pengiriman'];
    //         $data['id_user'] = $id_user;
    //         $data['ongkir'] = $ongkir;
    //         $data['status'] = 1;
    //         $data['tgl_order'] = date('Y-m-d h:i:s');
    //         $data['id_cabang'] = $this->session->userdata('id_cabang');

    //         if($this->db->insert('transaksi', $data)){

    //             foreach ($detail_order as $detail) {
    //                 $add['id_produk'] = $detail['id_produk'];
    //                 $add['harga'] = $detail['harga'];
    //                 $add['qty'] = $detail['qty'];
    //                 $add['sub_total'] = $detail['subtotal'];

    //                 $this->db->insert('transaksi_detail',$add);
    //             }

    //             $this->db->delete('transaksi_temp', array('id_user' => $id_user));
    //             $this->db->delete('transaksi_temp_detail', array('id_user' => $id_user));
    //         }
    // }

    public function save_alamat(){
        if($post = $this->input->post()){
            $data['nama'] = $post['nama'];
            $data['id_kota'] = $post['id_kota'];
            $data['id_provinsi'] = $post['id_provinsi'];
            $data['id_kecamatan'] = $post['id_kecamatan'];
            $data['id_kelurahan'] = $post['id_kelurahan'];
            $data['kode_pos'] = $post['kode_pos'];
            $data['alamat'] = $post['alamat'];
            $data['id_user'] = $this->session->userdata('id_user');

            if($this->db->insert('alamat_pengiriman', $data)){
                echo 1;
            }
        }
    }





}