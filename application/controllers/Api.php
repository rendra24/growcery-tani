<?php
header('Access-Control-Allow-Origin: *');  

class Api extends CI_Controller {

 public function __construct()
 {
  parent::__construct();
  $this->load->helper('global_helper');
  $this->load->model('web_model');
}


public function notifikasi(){
  $id_user = $this->input->post('id_user');
  $this->db->order_by('id','DESC');
  $get = $this->db->get_where('notifikasi', array('id_user' => $id_user))->result_array();

  foreach ($get as $row) {
    if($row['baca'] == 0){
      $notif = 'notif-aktif';
    }else{
      $notif = 'notif-read';
    }
    echo ' <li>
    <a href="/detail_notif/'.$row['id'].'/" class="item-link item-content '.$notif.'">
    <div class="item-inner">
    <div class="item-title-row">
    <div class="item-title">'.$row['judul'].'</div>
    <div class="item-after" style="font-size:10px;">'.datetime_indo($row['tanggal']).'</div>
    </div>
    <div class="item-text">'.$row['isi'].'</div>
    </div>
    </a>
    </li>';
  }
}

public function notifikasi_detail(){
 $id_user = $this->input->post('id_user');
 $id = $this->input->post('id');

 $add['baca'] = 1;
 if($this->db->update('notifikasi', $add , array('id' => $id))){
   $get = $this->db->get_where('notifikasi', array('id_user' => $id_user, 'id' => $id))->row_array();

   echo json_encode($get);
 }

}


public function change_password(){

  if($post = $this->input->post()){

   $cek = $this->db->get_where('users',  array('id' => $post['id_user']));

   if($cek->num_rows() > 0){
    $row = $cek->row_array();

    if (password_verify($post['password_lama'], $row['password'])) {

      $add['password'] = password_hash($post['password_baru'], PASSWORD_BCRYPT);

      if($this->db->update('users', $add , array('id' => $post['id_user']))){

        $json['success'] = true;
        $json['message'] = 'Password berhasil di ubah';

      }

    }else{
      $json['success'] = false;
      $json['message'] = 'Password anda salah';
    }

    echo json_encode($json);
  }

}

}

public function get_notif_user(){
 if($post = $this->input->post()){

  $urut = str_replace('"', '', $post['id']);
  $urut = str_replace("'", "", $urut);

  $urut = htmlspecialchars($urut);

  $cek = $this->db->get_where('notifikasi', array('id_user' => $post['id_user'], 'urut' => $urut))->row_array();
  $json['id'] = $cek['id'];
  echo json_encode($json);
}
}


public function get_profile(){
  if($post = $this->input->post()){
    $id_user = $post['id_user'];
    $get = $this->db->get_where('users',  array('id' => $post['id_user']))->row_array();

    $json['nama'] = $get['nama'];
    $json['tgl_lahir'] = $get['tgl_lahir'];
    $json['jenis_kelamin'] = $get['jenis_kelamin'];
    echo json_encode($json);
  }
}

public function update_profile(){
  if($post = $this->input->post()){
    $upp['nama'] = $post['nama'];
    $upp['tgl_lahir'] = $post['tgl_lahir'];
    $upp['jenis_kelamin'] = $post['jenis_kelamin'];

    $id_user = $post['id_user'];

    if($this->db->update('users', $upp, array('id' => $id_user))){
      $json['success'] = true;
      $json['message'] = 'Profil berhasil di ubah';

      echo json_encode($json);
    }
  }
}

public function update_player(){
   if($post = $this->input->post()){
      $cek = $this->db->get_where('users', array('id' => $post['id_user']))->row_array();

      if($cek['push_id'] == ''){
          $add['push_id'] = $post['player_id'];
          if($this->db->update('users', $add, array('id' => $id_user))){

          }
      }

      if(is_null($cek['push_id'])){
        $add['push_id'] = $post['player_id'];
          if($this->db->update('users', $add, array('id' => $id_user))){

          }
      }
   }
}



public function slider(){
  $get = $this->db->get_where('slider', array('flag' => 1))->result_array();
  foreach ($get as $row) {
    echo '<div class="swiper-slide"><img src="https://growcery.id/foto/slider/'.$row['gambar'].'"></div>';
  }
}
public function cabang(){
  $this->db->group_by("kota");
  $get = $this->db->get_where('cabang', array('flag' => 1))->result_array();
  foreach ($get as $row) {
    echo ' <li>
    <label class="item-radio item-content">
    <input type="radio" name="cabang" class="cabang"  value="'.$row['id'].'" cabang="'.$row['kota'].'" />
    <i class="icon icon-radio"></i>
    <div class="item-inner">
    <div class="item-title">'.$row['kota'].'</div>
    </div>
    </label>
    </li>';
  }
}

public function cabang_detail(){
  $id = $this->input->post('id');
  $get = $this->db->query("SELECT id,nama_kantor FROM cabang  WHERE flag=1 AND kota=(SELECT kota FROM cabang WHERE id='".$id."') ")->result_array();
  foreach ($get as $row) {
    echo ' <li>
    <label class="item-radio item-content">
    <input type="radio" name="cabang" class="cabang_detail"  value="'.$row['id'].'" cabang="'.$row['nama_kantor'].'" />
    <i class="icon icon-radio"></i>
    <div class="item-inner">
    <div class="item-title">'.$row['nama_kantor'].'</div>
    </div>
    </label>
    </li>';
  }
}

public function do_login(){

  if($post = $this->input->post()){

    $email = $post['email'];
    $userpass = $post['password'];



    $cek = $this->db->get_where('users',  array('email' => $email));

    if($cek->num_rows() > 0){
      $row = $cek->row_array();

      if (password_verify($userpass, $row['password'])) {
        if($row['flag'] == 1){
         $newdata = array(
          'id_user'   => $row['id'],
          'nama'      => $row['nama'],
          'email'     => $row['email'],
          'logged_in' => TRUE
        );

         $upp['push_id'] = $post['player_id'];
         $this->db->update('users',$upp, array('id' => $row['id']));

         echo json_encode($newdata);
       }else{
         $json['logged_in'] = false;
         $json['message']  = "Akun anda belum aktif";

         echo json_encode($json);
       }
     }else{
      $json['logged_in'] = false;
      $json['message']  = "password atau email salah";

      echo json_encode($json);
    }

  }else{
   $json['logged_in'] = false;
   $json['message']  = "password atau email salah";

   echo json_encode($json);
 }
}else{
 $json['logged_in'] = false;
 $json['message']  = "Error Server";

 echo json_encode($json);
}
}


public function do_login_google(){

  if($post = $this->input->post()){
   $email = $post['email'];
   $player_id = $post['player_id'];

   // $email = 'rendrasaputra244@gmail.com';
   // $player_id = '';
   // $post['nama'] = 'rendra septian';
   // $post['email'] = 'rendrasaputra244@gmail.com';
   // $post['player_id'] = '';


   $cek = $this->db->get_where('users',  array('email' => $email));

   if($cek->num_rows() > 0){
    $row = $cek->row_array();

    if($row['flag'] == 1){
     $newdata = array(
      'id_user'   => $row['id'],
      'nama'      => $row['nama'],
      'email'     => $row['email'],
      'logged_in' => TRUE
    );

     $upp['push_id'] = $player_id;
     $upp['emailVerified'] = 1;
     $this->db->update('users',$upp, array('id' => $row['id']));

     echo json_encode($newdata);
   }else{
     $json['logged_in'] = false;
     $json['message']  = "Akun anda belum aktif";

     echo json_encode($json);
   }



 }else{
  $upp['nama'] = $post['nama'];
  $upp['email'] = $post['email'];
  $upp['push_id'] = $player_id;
  $upp['emailVerified'] = 1;
  $upp['flag'] = 1;
  $upp['login_google'] = 1;


  if($this->db->insert('users',$upp)){


   $newdata = array(
    'id_user'   => $this->db->insert_id(),
    'nama'      => $post['nama'],
    'email'     => $post['email'],
    'logged_in' => TRUE
  );

   echo json_encode($newdata);

 }


}

}

}


public function do_regis(){
  if($post = $this->input->post()){

    $cek = $this->db->get_where('users', array('email' => $post['email']));

    if($cek->num_rows() > 0){
      $json['message'] =  "Email sudah di gunakan";
      echo json_encode($json);
    }else{
      $data['email'] = $post['email'];
      $data['nama'] = $post['nama'];
      $data['telephone'] = $post['hp'];
      $data['password'] = password_hash($post['password'], PASSWORD_BCRYPT);


      if($this->db->insert('users', $data)){
        $newdata = array(
          'id_user'   => $this->db->insert_id(),
          'nama'      => $post['nama'],
          'email'     => $post['email'],
          'logged_in' => TRUE
        );

        echo json_encode($newdata);
      } 
    }


  }
}


public function update_checout(){

  $post = $this->input->post();

  $id_user = $this->input->post('id_user');
  $transaksi_temp = $this->db->query('SELECT * from transaksi_temp WHERE  status =1  AND id_user="'.$id_user.'" OR status IS NULL  AND id_user="'.$id_user.'" ORDER BY id DESC LIMIT 1 ');
  $transaksi_temp = $transaksi_temp->row_array();
  $id_transaksi = $transaksi_temp['id'];

  $cek = $this->db->get_where('transaksi_temp', array('id' => $id_transaksi));


  if($cek->num_rows() == 0){

    $add['id_user'] = $id_user;
    $add['status'] = 1;
    $data['id_user'] = $id_user;

    $this->db->insert('transaksi_temp', $add);

  }else{
    $temp_detail = $this->db->query('SELECT SUM(sub_total) as total FROM transaksi_temp_detail WHERE id_transaksi = "'.$id_transaksi.'" ')->row_array();

     if((int)$temp_detail['total'] >= 100000 && (int)$temp_detail['total'] < 150000){
                $ongkir = 15000;
            }elseif((int)$temp_detail['total'] >= 150000){
                $ongkir = 0;
            }else{
                $ongkir = 20000;
            }

    $json['ongkir'] = $ongkir;
    $add['grand_total'] = $temp_detail['total'];
    $add['ongkir'] = $ongkir;
    $add['pembayaran'] = $ongkir + $temp_detail['total']; 
    $add['id_user'] = $id_user;
    $add['status'] = 1;

    $this->db->update('transaksi_temp', $add, array('id' => $id_transaksi));
    
  }

  $json['id_transaksi'] = $id_transaksi;
  $json['success'] = true;

  echo json_encode($json);

}

public function cart(){
 $id_user = $this->input->post('id_user');
 
 $sql = "SELECT MAX(id) as id_transaksi from  transaksi_temp 
 WHERE id_user='$id_user' AND status = 1 OR id_user='$id_user' AND status IS NULL ";

 $get_id = $this->db->query($sql)->row_array();
$id_transaksi = $get_id['id_transaksi'];

 $query_p = "SELECT a.*, b.nama_produk,b.gambar from transaksi_temp_detail as a 
 LEFT JOIN produk as b ON a.id_produk = b.id 
 LEFT JOIN transaksi_temp c ON a.id_transaksi = c.id
 WHERE c.id= '".$id_transaksi."' GROUP BY a.id_produk";
 $shop = $this->db->query($query_p);

 if($shop->num_rows() > 0){
  $shop = $shop->result_array(); 
  $grand_total = 0;

  foreach ($shop as $row) {
    $grand_total += $row['sub_total'];


    echo '<li style="border-bottom:1px solid #DDD;margin-bottom:15px;padding-bottom:15px;font-size:12px;">
    <div class="row" style="border-bottom:0px;padding-left:15px;">
    <div class="col-30" style="text-align:center;"><img src="https://growcery.id/foto/produk/'.$row['gambar'].'" style="height:60px;width:auto;max-width:100%;" /></div>
      
      <div class="col-40" style="border-bottom:0px;"> 
        <div>'.$row['nama_produk'].'</div>
        <div>'.$row['qty'].' X <span class="hitung-price" key="'.$row['sub_total'].'" >'.rupiah($row['harga']).'</span></div>
      </div>

      <div class="col-30" style="text-align:right;">
        <div class="delete_temp_cart" key="'.$row['id'].'" style="margin-bottom:15px;"><span class="material-icons" style="font-size:14px;">delete</span></div>
        <a class="edit_qty_item" href="#"  key="'.$row['id_transaksi'].'" produk="'.$row['id_produk'].'" style="font-size:10px;color:blue;">Edit Jumlah</div>
      </a>

    </div>

    <div class="row" style="padding-left: 15px;padding-right: 15px;font-size:12px;">
    <div class="col">
    Jumlah
    </div>
    <div class="col" style="text-align: right;">
    <p style="font-weight:bold;margin:0px;">'.rupiah($row['sub_total']).'</p>
    </div>

    </div>
    </li>';

  }

}
}


public function get_detail_history()
{

  $id_transaksi = $this->input->post('id_transaksi');
  $status = $this->input->post('status');
  if($status == 'selesai'){
        $temp = $this->db->query("SELECT a.*, a.tgl_kirim as tgl_pengiriman ,  b.nama, b.alamat, b.nama , c.nama as provinsi, d.nama as kota, e.nama as kecamatan  FROM transaksi a 
                                    LEFT JOIN alamat_pengiriman b ON a.id_pengiriman = b.id
                                    LEFT JOIN provinsi c ON b.id_provinsi = c.id_prov
                                    LEFT JOIN kabupaten d ON b.id_kota = d.id_kab
                                    LEFT JOIN kecamatan e ON b.id_kecamatan = e.id_kec
                                    WHERE a.id = '".$id_transaksi."' ")->row_array();

        $detail = $this->db->query("SELECT a.*, b.nama_produk ,  b.gambar FROM transaksi_detail a 
                                    LEFT JOIN produk b ON a.id_produk = b.id
                                    WHERE a.id_transaksi = '".$id_transaksi."' ")->result_array();

          $produk = array();
  foreach ($detail as $row) {
    $produk[] = array('id_produk' => $row['id_produk'], 'sub_total' => rupiah($row['sub_total']), 'qty' => $row['qty'], 'harga' => rupiah($row['harga']), 'gambar' => $row['gambar'], 'nama_produk' => $row['nama_produk']);

  }

  $json['id_transaksi'] = $temp['id'];
  $json['kode_transaksi'] = $temp['kode_transaksi'];
  $json['pembayaran'] = rupiah($temp['grand_total'] + $temp['ongkir']);
  $json['senin'] = date_indo($temp['tgl_pengiriman']);
  $json['ongkir'] = rupiah($temp['ongkir']);
  $json['grand_total'] = rupiah($temp['grand_total']);
  $json['data'] = $produk;


  echo json_encode($json);
    

       
}else{

  $temp = $this->db->query("SELECT a.*, b.atas_nama , b.no_rekening , b.logo from transaksi_temp a
    LEFT JOIN data_bank b ON a.id_bank_pembayaran = b.id
    WHERE a.id='".$id_transaksi."' ")->row_array();

  $detail = $this->db->query("SELECT a.*, b.nama_produk , b.gambar  from transaksi_temp_detail a 
    LEFT JOIN produk b ON a.id_produk = b.id
    WHERE id_transaksi='".$id_transaksi."' ")->result_array();

  $produk = array();
  foreach ($detail as $row) {
    $produk[] = array('id_produk' => $row['id_produk'], 'sub_total' => rupiah($row['sub_total']), 'qty' => $row['qty'], 'harga' => rupiah($row['harga']), 'gambar' => $row['gambar'], 'nama_produk' => $row['nama_produk']);

  }

    $json['id_transaksi'] = $temp['id'];
  $json['kode_transaksi'] = $temp['kode_transaksi'];
  $json['pembayaran'] = rupiah($temp['pembayaran']);
  $json['senin'] = date_indo($temp['tgl_pengiriman']);
  $json['atas_nama'] = $temp['atas_nama'];
  $json['no_rekening'] = $temp['no_rekening'];
  $json['logo'] = $temp['logo'];
  $json['ongkir'] = rupiah($temp['ongkir']);
  $json['grand_total'] = rupiah($temp['grand_total']);
  $json['data'] = $produk;


  echo json_encode($json);
    

}



} 

public function load_alamat(){

  if($this->input->post('id_user')){
    $id_user = $this->input->post('id_user');
    $sql = "SELECT a.*, b.nama as nama_prov, c.nama as nama_kota, d.nama as kecamatan from alamat_pengiriman a 
    LEFT JOIN provinsi b ON a.id_provinsi=b.id_prov
    LEFT JOIN kabupaten c ON a.id_kota=c.id_kab
    LEFT JOIN kecamatan d ON a.id_kecamatan=d.id_kec
    WHERE a.id_user='".$id_user."'
    ";
    $alamat = $this->db->query($sql)->result_array();

    foreach ($alamat as $row) {


      echo '<div class="card" style="margin-top: 15px;">
      <div class="card-content" style="padding: 10px;">
      <div class="row">
      <div class="col-10" style="text-align: center;">
      <i class="fa fa-map-marker"></i>
      </div>
      <div class="col-md-70">
      <p style="margin-bottom: 0px;font-size:12px;"><b>'.$row['nama'].'</b></p>
      <p style="margin-bottom: 0px;font-size:12px;">'.$row['alamat'].' Provinsi '.$row['nama_prov'].', '.$row['nama_kota'].' Kecamatan'.$row['kecamatan'].'</p>
      </div>
      <div class="col-md-20" style="text-align: right;padding-top: 15px;">
      <button type="button" class="button button-fill button-small color-blue btn-pilih" key="'.$row['id'].'">Pilih</button>
      </div>
      </div>
      </div>
      </div>';


    }   
  }

}

public function update_pengiriman(){
  $id_transaksi = $this->input->post('id_transaksi');

  $add['id_pengiriman'] = $this->input->post('id_pengiriman');
  if($this->db->update('transaksi_temp',$add , array('id' => $id_transaksi))){
    $json['success'] = true;
    echo json_encode($json);
  }
}

public function get_tgl_pengiriman(){
  
  $tgl = $this->web_model->hari_pengiriman();
  $display = array();
  $value = array();
  $tomorrow = date('Y-m-d',strtotime(date('Y-m-d') . "+1 days"));
  $tgl_besok = date('Y-m-d');
  foreach ($tgl as $row) {
   if($row != 'tidak aktif'){
              if(date('Y-m-d') != $row && date('Y-m-d') < $row){
              
              if(date('H:i:s') >= '16:00:00'){
                if($tomorrow == $row){
                  $tgl_besok = $row;
                }
              }

              if($tgl_besok != $row && $tgl_besok != ''){

                $display[] = date_indo($row);
                $value[] = $row;
              }
              
              }
    }
  
  }

  $data['tgl'] = $value;
  $data['display'] = $display;
  echo json_encode($data);
}

public function get_data_checkout(){
 $id_transaksi = $this->input->post('id_transaksi');
 $temp = $this->db->query("SELECT a.*,b.nama, b.alamat, b.nama , c.nama as provinsi, d.nama as kota, e.nama as kecamatan  from transaksi_temp a
   LEFT JOIN alamat_pengiriman b ON a.id_pengiriman = b.id
   LEFT JOIN provinsi c ON b.id_provinsi = c.id_prov
   LEFT JOIN kabupaten d ON b.id_kota = d.id_kab
   LEFT JOIN kecamatan e ON b.id_kecamatan = e.id_kec
   WHERE a.id='".$id_transaksi."' ")->row_array();

 $detail = $this->db->query("SELECT a.*, b.nama_produk , b.gambar  from transaksi_temp_detail a 
  LEFT JOIN produk b ON a.id_produk = b.id
  WHERE a.id_transaksi='".$id_transaksi."' ")->result_array();

 $produk = array();
 
 foreach ($detail as $row) {
  $produk[] = array('id_produk' => $row['id_produk'], 'sub_total' => rupiah($row['sub_total']), 'qty' => $row['qty'], 'harga' => rupiah($row['harga']), 'gambar' => $row['gambar'], 'nama_produk' => $row['nama_produk']);


}

$json['id_transaksi'] = $temp['id'];
$json['kode_transaksi'] = $id_transaksi;
$json['ongkir'] = rupiah($temp['ongkir']);
$json['grand_total'] = rupiah($temp['grand_total']);
$json['pembayaran'] = rupiah($temp['pembayaran']);
$json['nama'] = $temp['nama'];

$json['data'] = $produk;

if($temp['id_pengiriman'] == 0){
  $json['alamat_pengiriman'] = '';
}else{
 $json['alamat_pengiriman'] =  $temp['alamat'].' , '.$temp['kecamatan'].' , '.$temp['kota'].' , '.$temp['provinsi'];
}

echo json_encode($json);
}

public function update_pembayaran(){

  if($post = $this->input->post()){

    $add['id_bank_pembayaran'] = $post['id_bank_pembayaran'];

    $id_transaksi = $post['id_transaksi'];

    if($this->db->update('transaksi_temp', $add ,  array('id' => $id_transaksi))){

    }

  }

}

public function update_status_pembayaran(){
 if($post = $this->input->post()){
  $id_transaksi = $post['id_transaksi'];
  $id_user = $post['id_user'];

  $cek = $this->db->query('SELECT * FROM transaksi_temp WHERE id = "'.$id_transaksi.'" AND id_bank_pembayaran IS NULL ');

  if($cek->num_rows() > 0){
    $json['success'] = false;
    $json['message'] = 'Pembayaran harus di pilih';

    echo json_encode($json);
  }else{
    $add['tgl_order'] = date('Y-m-d H:i:s');
    $add['kode_transaksi'] = 'TR/GC/'.date('Y/m/d/').$id_transaksi;
    $add['id_cabang'] = $post['id_cabang'];



    if($this->db->update('transaksi_temp', $add ,  array('id' => $id_transaksi, 'id_user' => $id_user))){

      $json['success'] = true;
      $json['message'] = 'Berhasil Memilih Pembayaran';

      echo json_encode($json);

    }
  }



}
}

public function update_konfirm(){
  if($post = $this->input->post()){
    $id_transaksi = $post['id_transaksi'];
    $id_user = $post['id_user'];
    $add['status'] = 2;


    if($this->db->update('transaksi_temp', $add ,  array('id' => $id_transaksi, 'id_user' => $id_user))){

    }

  }
}

public function get_data_pembayaran(){
  if($post = $this->input->post()){
    $id_transaksi = $post['id_transaksi'];

    $get = $this->db->query("SELECT a.*, b.nama_bank, b.no_rekening, b.atas_nama , b.logo FROM transaksi_temp a LEFT JOIN data_bank b ON a.id_bank_pembayaran = b.id WHERE a.id='".$id_transaksi."' ")->row_array();

    $json['kode_transaksi'] = $get['kode_transaksi'];
    $json['pembayaran'] = rupiah($get['pembayaran']);
    $json['nama_bank'] = $get['nama_bank'];
    $json['no_rekening'] = $get['no_rekening'];
    $json['atas_nama'] = $get['atas_nama'];
    $json['logo'] = $get['logo'];

    echo json_encode($json);

  }
}

public function load_bank(){

  $get= $this->db->get_where('data_bank', array('flag' => 1))->result_array();

  foreach ($get as $row) {

    echo '<li>
    <label class="item-radio item-radio-icon-start item-content">
    <input type="radio" class="btn-pilih-pembayaran" name="id_bank_pembayaran" value="'.$row['id'].'" />
    <i class="icon icon-radio"></i>
    <div class="item-inner">
    <div class="item-title-row">
    <div class="item-title">'.$row['nama_bank'].'</div>     
    </div>
    <div class="item-subtitle">'.$row['atas_nama'].' , '.$row['no_rekening'].'</div>
    </div>
    </label>
    </li>';

  }
}


public function total_belanja(){
  $id_user = $this->input->post('id_user');
  
  $sql = "SELECT MAX(id) as id_transaksi from  transaksi_temp 
 WHERE id_user='$id_user' AND status = 1 OR id_user='$id_user' AND status IS NULL ";

  $get_id = $this->db->query($sql)->row_array();
  $id_transaksi = $get_id['id_transaksi'];

  if($id_produk = $this->input->post('id_produk')){
    $total = $this->db->query("SELECT SUM(CASE WHEN a.qty >= 1 THEN qty ELSE 0 END) AS total , SUM(a.sub_total) as grand_total FROM transaksi_temp_detail a LEFT JOIN transaksi_temp b ON a.id_transaksi = b.id WHERE a.id_transaksi='".$id_transaksi."' AND a.id_produk='".$id_produk."'")->row_array();

    $data['qty'] = $total['total'];
    $data['total'] = rupiah($total['grand_total']);
    echo json_encode($data); 

  }else{
   $total = $this->db->query("SELECT SUM(CASE WHEN a.qty >= 1 THEN qty ELSE 0 END) AS total , SUM(a.sub_total) as grand_total FROM transaksi_temp_detail a LEFT JOIN transaksi_temp b ON a.id_transaksi = b.id WHERE a.id_transaksi='".$id_transaksi."' ")->row_array();

   $data['qty'] = $total['total'];
   $data['total'] = rupiah($total['grand_total']);
   echo json_encode($data);
 }
}
public function update_qty_cart(){
 if($post = $this->input->post()){
      $id_user = $post['id_user'];
      $id_produk = $post['id_produk'];
      $id_transaksi = $post['id'];
      $qty = $post['qty'];

      $produk = $this->db->query('SELECT If(promo=1,harga_promo,harga_normal) as harga FROM produk WHERE id = "'.$id_produk.'" ')->row_array();

      $upp['qty'] = $qty;
      $upp['sub_total'] = $qty * (int)$produk['harga'];
      if($this->db->update('transaksi_temp_detail',$upp, array('id_transaksi' => $id_transaksi, 'id_produk' => $id_produk))){

      }
 }
}
public function add_to_cart(){
  if($post = $this->input->post()){
    $id_user = $post['id_user'];
    $get = $this->db->query('SELECT * FROM transaksi_temp WHERE id_user ="'.$id_user.'" AND status =1 OR id_user ="'.$id_user.'" AND status IS NULL ORDER BY id DESC LIMIT 1');

    if($get->num_rows() > 0){

      $get = $get->row_array();
      $id_transaksi = $get['id'];                   

    }else{

      $add['id_user'] = $id_user;

      $this->db->insert('transaksi_temp',$add);
      $id_transaksi = $this->db->insert_id();                


    }



    $produk = $this->db->query('SELECT If(promo=1,harga_promo,harga_normal) as harga FROM produk WHERE id = "'.$post['id_produk'].'" ')->row_array();

    $cek = $this->db->get_where('transaksi_temp_detail', array('id_produk' => $post['id_produk'], 'id_user' => $id_user, 'id_transaksi' => $id_transaksi));

    if($cek->num_rows() > 0){
      $cek = $cek->row_array();
      $data['sub_total'] = ($cek['qty'] + $post['qty']) * (int)$produk['harga'];
      $data['qty'] = $cek['qty'] + $post['qty'];

      if($this->db->update('transaksi_temp_detail', $data , array('id_produk' => $post['id_produk'], 'id_user' => $id_user,  'id_transaksi' => $id_transaksi))){
                    //echo $this->db->last_query();

      }
    }else{
      $data['id_transaksi'] = $id_transaksi;
      $data['id_user'] = $id_user;
      $data['id_produk'] = $post['id_produk'];
      $data['harga'] = $produk['harga'];
      $data['qty'] = $post['qty'];
      $data['sub_total'] = (int)$produk['harga'] * (int)$post['qty'];


      if($this->db->insert('transaksi_temp_detail', $data)){
                     //echo $this->db->last_query();                  
      } 
    }



  }
}

public function delete_cart(){
  $id = $this->input->post('id');
  $id_user = $this->input->post('id_user');
        //$id= 4;

  if($this->db->delete('transaksi_temp_detail', array('id_user' => $id_user, 'id' => $id))){


  }
}


public function minus_cart(){

  $id_produk = $this->input->post('id_produk');
  $id_user = $this->input->post('id_user');

  $get = $this->db->query('SELECT * FROM transaksi_temp WHERE id_user ="'.$id_user.'" AND status =1 OR id_user ="'.$id_user.'" AND status IS NULL');

  if($get->num_rows() > 0){

    $get = $get->row_array();
    $id_transaksi = $get['id']; 


    $get = $this->db->get_where('transaksi_temp_detail', array('id_produk' => $id_produk, 'id_transaksi' => $id_transaksi));

    if($get->num_rows() > 0){
      $qty = $get->row_array();

      if($qty['qty'] > 1){
        $upp['qty'] = $qty['qty'] - 1;
        $this->db->update('transaksi_temp_detail',$upp, array('id_transaksi' => $id_transaksi,'id_produk' => $id_produk));
      }else{
        $this->db->delete('transaksi_temp_detail', array('id_transaksi' => $id_transaksi,'id_produk' => $id_produk));
      }

    }

  }   

}

public function get_cart_total_awal(){
 $id_user = $this->input->post('id_user');
         //$id_user = 1;
 $get = $this->db->query('SELECT * FROM transaksi_temp WHERE id_user ="'.$id_user.'" AND status =1 OR id_user ="'.$id_user.'" AND status IS NULL ORDER BY id DESC LIMIT 1');

 if($get->num_rows() > 0){

  $get = $get->row_array();
  $id_transaksi = $get['id']; 

  $get = $this->db->get_where('transaksi_temp_detail', array('id_transaksi' => $id_transaksi))->result_array();

  echo json_encode($get);
}
}

public function history_pesanan(){

  $id_user = $this->input->post('id_user');
  $cek_order = $this->db->query("SELECT a.* FROM transaksi_temp a  WHERE a.id_user='".$id_user."' AND a.status='2' ");

  if($cek_order->num_rows() > 0){
    $cek_order = $cek_order->result_array();

    foreach ($cek_order as $row) {

      echo '<a href="/detail_pesanan/'.$row['id'].'/proses/" style="color:#000;"><div class="card">
      <div class="card-content" style="padding: 8px;">
      <p style="font-size: 12px;margin-top: 0px;margin-bottom: 0px;">'.$row['kode_transaksi'].'</p>
      <p style="font-size: 12px;font-weight: bold;margin-top: 5px;margin-bottom:0px;">'.rupiah($row['pembayaran']).'</p>
      <div style="padding: 5px; border-radius: 5px; background-color: #3fb8f4; color: #FFF;font-size:10px;width:148px;margin-bottom:5px;margin-top:5px;">Menunggu Verifikasi Pembayaran</div>
      <div class="row">
      <div class="col-40"><p class="text-p">Tanggal Pesanan</p></div>
      <div class="col-60" style="text-align: right;"><p class="text-p" style="font-weight: 600;">'.datetime_indo($row['tgl_order']).'</p></div>
      </div>
      </div>
      </div></a>';
    }
  }
}

public function history_pesanan_selesai(){
  $id_user = $this->input->post('id_user');
  $cek_selesai = $this->db->query("SELECT a.* FROM transaksi a  WHERE a.id_user='".$id_user."' AND a.status='1' ");

  if($cek_selesai->num_rows() > 0){
    $cek_selesai = $cek_selesai->result_array();

    foreach ($cek_selesai as $row) {

     echo '<a href="/detail_pesanan/'.$row['id'].'/selesai/" style="color:#000;"><div class="card">
     <div class="card-content" style="padding: 8px;">
     <p style="font-size: 12px;margin-top: 0px;margin-bottom: 0px;">'.$row['kode_transaksi'].'</p>
     <p style="font-size: 12px;font-weight: bold;margin-top: 5px;margin-bottom:0px;">'.rupiah($row['grand_total'] + $row['ongkir']).'</p>
     <div style="padding: 5px; border-radius: 5px; background-color: #f4d03f; color: #FFF;font-size:10px;width:82px;margin-bottom:5px;margin-top:5px;">Proses Pengiriman</div>
     <div class="row">
     <div class="col-40"><p class="text-p">Tanggal Pesanan</p></div>
     <div class="col-60" style="text-align: right;"><p class="text-p" style="font-weight: 600;">'.datetime_indo($row['tgl_order']).'</p></div>
     </div>
     </div>
     </div></a>';
   }
 }
}

public function history_pesanan_batal(){
   $id_user = $this->input->post('id_user');
  $cek_selesai = $this->db->query("SELECT a.* FROM transaksi a  WHERE a.id_user='".$id_user."' AND a.status='0' ");

    $cek_selesai = $cek_selesai->result_array();

    foreach ($cek_selesai as $row) {

     echo '<div class="card">
     <div class="card-content" style="padding: 8px;">
     <p style="font-size: 12px;margin-top: 0px;margin-bottom: 0px;">'.$row['kode_transaksi'].'</p>
     <p style="font-size: 12px;font-weight: bold;margin-top: 5px;">'.rupiah($row['grand_total'] + $row['ongkir']).'</p>
     <div class="row">
     <div class="col-40"><p class="text-p">Tanggal Pesanan</p></div>
     <div class="col-60" style="text-align: right;"><p class="text-p" style="font-weight: 600;">'.datetime_indo($row['tgl_order']).'</p></div>
     </div>
     </div>
     </div>';
   }


     $cek_temp = $this->db->query("SELECT a.* FROM transaksi_temp a  WHERE a.id_user='".$id_user."' AND a.status='0' ");

    $cek_batal = $cek_temp->result_array();

    foreach ($cek_batal as $row) {

     echo '<div class="card">
     <div class="card-content" style="padding: 8px;">
     <p style="font-size: 12px;margin-top: 0px;margin-bottom: 0px;">'.$row['kode_transaksi'].'</p>
     <p style="font-size: 12px;font-weight: bold;margin-top: 5px;">'.rupiah($row['grand_total'] + $row['ongkir']).'</p>
     <div class="row">
     <div class="col-40"><p class="text-p">Tanggal Pesanan</p></div>
     <div class="col-60" style="text-align: right;"><p class="text-p" style="font-weight: 600;">'.datetime_indo($row['tgl_order']).'</p></div>
     </div>
     </div>
     </div>';
   }
 
}

public function kategori()
{
  $kategori = $this->db->get('kategori')->result_array();

  echo '<div class="image-kategori">
  <a href="/produk/0/Semua Produk/" style="color: #000;width: 100%;height: 100%;">
  <div style="background-color:#b8bbcc;background-image:url(https://tanihub.com/static/icons/all-products.svg);    height: 50px;
  background-size: cover;
  background-position: center;border-radius: 10px;"></div>
  <p style="font-size: 12px;margin-bottom: 0px;">Semua</p>
  </a>
  </div>';

  foreach ($kategori as $row) {
   echo '<div class="image-kategori">
   <a href="/produk/'.$row['id'].'/'.$row['nama_kategori'].'/" style="color: #000;width: 100%;height: 100%;">
   <div style="background-color:#b8bbcc;background-image:url(https://growcery.id/foto/produk/'.$row['gambar'].');    height: 50px;
   background-size: cover;
   background-position: center;border-radius: 10px;"></div>
   <p style="font-size: 12px;margin-bottom: 0px;">'.$row['alias'].'</p>
   </a>
   </div>';
 }
}

public function produk_kategori($id_kategori,$nama_kategori)
{
    //$id_kategori = htmlspecialchars($this->input->post('id_kategori'));
  $query = "SELECT a.*,  b.nama_satuan , c.status  from produk as a 
  LEFT JOIN satuan b ON a.id_satuan = b.id
  LEFT JOIN produk_status c ON a.id_status = c.id
  WHERE a.flag=1 AND a.id_kategori='".$id_kategori."' LIMIT 10 ";
  $produk = $this->db->query($query)->result_array();

  echo '<div class="row" style="width: 100%;margin-bottom: 15px;margin-top: 15px;">
  <div class="col-50">
  <p style="margin: 0px;font-weight: bold;font-size: 14px;">'.$nama_kategori.'</p>
  </div>
  <div class="col-50" style="text-align: right;">
  <a href="/produk/'.$id_kategori.'/'.$nama_kategori.'/" >
  <p style="margin: 0px;font-size: 14px;" class="text-color-blue">Lihat Semua</p>
  </a>
  </div>
  </div>
  <div class="container-slide">';

  foreach ($produk as $row) {

    if($row['promo'] == 1){
      $save = (($row['harga_normal'] - (int)$row['harga_promo']) / $row['harga_normal']) * 100;

      $save = floor($save);

      $harga = '<div style="font-size:12px;"><del>'. rupiah($row['harga_normal']).'</del><span class="badge color-green">Save '.$save.'%</span></div><span style="color:red;font-size:12px;">'.rupiah($row['harga_promo']).'</span>';



    }else{
      $harga = rupiah((int)$row['harga_normal']);
    }

    echo '<div class="items">

    <div  style="background:url(https://growcery.id/foto/produk/'.$row['gambar'].');background-size:cover;background-position:center;" 
    class="card-header align-items-flex-end img-items produk-'.$row['id'].'">
    <div class="status_produk">'.$row['status'].'</div>
    <div class="cardbeli-'.$row['id'].' atur-bg" style="display:none;">
    <p class="text_cart"><span class="totalcart-'.$row['id'].'">1</span><br> Item</p>
    </div>
    </div> 



    <div style="padding:8px;padding-top:0px;">
    <p class="p-title" style="color:#000;min-height:34px;max-height:34px;"><b>'.$row['nama_produk'].'</b></p> 
    <p class="p-desc" style="margin-bottom: 10px;min-height:29px;">'.substr($row['keterangan'], 0, 30).'</p>
    <div style="min-height:40px;max-height:40px;"><b>'.$harga.'</b></div>


    <button type="button" class="button button-fill button-small btn-add-temp btn-add-temp-'.$row['id'].'" key="'.$row['id'].'" style="width:100%;">Beli</button>
    <div class="row btnqty-'.$row['id'].'" style="display:none;">
    <button type="button" class="col button button-fill button-small color-red btn-minus" key="'.$row['id'].'">-</button>
    <button type="button" class="col button button-fill button-small btn-plus" key="'.$row['id'].'">+</button>
    </div>
    </div> 

    </div>';


  }

  echo '</div>';

}

public function produk_pilihan()
{
    //$id_kategori = htmlspecialchars($this->input->post('id_kategori'));
  $query = "SELECT a.*,  b.nama_satuan , c.status  from produk as a 
  LEFT JOIN satuan b ON a.id_satuan = b.id
  LEFT JOIN produk_status c ON a.id_status = c.id
  WHERE a.flag=1 AND a.favorit=1 LIMIT 10 ";
  $produk = $this->db->query($query)->result_array();

  echo '<div class="row" style="width: 100%;margin-bottom: 15px;margin-top: 15px;">
  <div class="col-100">
  <p style="margin: 0px;font-weight: bold;font-size: 14px;">Produk Pilihan</p>
  </div>

  </div>
  <div class="container-slide">';

  foreach ($produk as $row) {

    if($row['promo'] == 1){
      $save = (($row['harga_normal'] - (int)$row['harga_promo']) / $row['harga_normal']) * 100;

      $save = floor($save);

      $harga = '<div style="font-size:12px;"><del>'. rupiah($row['harga_normal']).'</del><span class="badge color-green">Save '.$save.'%</span></div><span style="color:red;font-size:12px;">'.rupiah($row['harga_promo']).'</span>';



    }else{
      $harga = rupiah((int)$row['harga_normal']);
    }

    echo '<div class="items">

    <div  style="background:url(https://growcery.id/foto/produk/'.$row['gambar'].');background-size:cover;background-position:center;" 
    class="card-header align-items-flex-end img-items produk-'.$row['id'].'">
    <div class="status_produk">'.$row['status'].'</div>
    <div class="cardbeli-'.$row['id'].' atur-bg" style="display:none;">
    <p class="text_cart"><span class="totalcart-'.$row['id'].'">1</span><br> Item</p>
    </div>
    </div> 



    <div style="padding:8px;padding-top:0px;">
    <p class="p-title" style="color:#000;min-height:34px;max-height:34px;"><b>'.$row['nama_produk'].'</b></p> 
    <p class="p-desc" style="margin-bottom: 10px;min-height:29px;">'.substr($row['keterangan'], 0, 30).'</p>
    <div style="min-height:40px;max-height:40px;"><b>'.$harga.'</b></div>


    <button type="button" class="button button-fill button-small btn-add-temp btn-add-temp-'.$row['id'].'" key="'.$row['id'].'" style="width:100%;">Beli</button>
    <div class="row btnqty-'.$row['id'].'" style="display:none;">
    <button type="button" class="col button button-fill button-small color-red btn-minus" key="'.$row['id'].'">-</button>
    <button type="button" class="col button button-fill button-small btn-plus" key="'.$row['id'].'">+</button>
    </div>
    </div> 

    </div>';


  }

  echo '</div>';

}


public function produk(){
  if($post = $this->input->post()){
    $id = $post['id'];
    $row = $this->db->get_where('produk',array('id' => $id))->row_array();

    $data['gambar'] = 'https://growcery.id/foto/produk/'.$row['gambar'];
    $data['nama_produk'] = $row['nama_produk'];
    $data['harga'] = rupiah($row['harga_normal']);
    $data['keterangan'] = $row['keterangan'];

    echo json_encode($data);
  }
}

public function produk_cart(){
   if($post = $this->input->post()){
    $id = $post['id'];
    $id_produk = $post['id_produk'];
    $row = $this->db->query('SELECT a.*, b.gambar, b.nama_produk, b.keterangan   FROM  transaksi_temp_detail a LEFT JOIN produk b ON a.id_produk = b.id WHERE a.id_produk="'.$id_produk.'" AND a.id_transaksi="'.$id.'"')->row_array();

    $data['gambar'] = 'https://growcery.id/foto/produk/'.$row['gambar'];
    $data['nama_produk'] = $row['nama_produk'];
    $data['keterangan'] = $row['keterangan'];
    $data['qty'] = $row['qty'];

    echo json_encode($data);
  }
}

public function produk_by_kategori(){

  $id_kategori = htmlspecialchars($this->input->post('id'));
  $cari = htmlspecialchars($this->input->post('cari'));

  $sql_plus= '';
  if($id_kategori != 0){
    $sql_plus = " AND a.id_kategori='".$id_kategori."' ";
  }


  $query = "SELECT a.*,  b.nama_satuan , c.status from produk as a 
  LEFT JOIN satuan b ON a.id_satuan = b.id 
  LEFT JOIN produk_status c ON a.id_status = c.id
  WHERE a.flag=1 ".$sql_plus." AND a.nama_produk LIKE '%".$cari."%'";
  
  $produk = $this->db->query($query)->result_array();



  foreach ($produk as $row) {

    if($row['promo'] == 1){
      $save = (($row['harga_normal'] - $row['harga_promo']) / $row['harga_normal']) * 100;

      $save = floor($save);

      $harga = '<div style="font-size:12px;"><del>'. rupiah($row['harga_normal']).'</del><span class="badge color-green">Save '.$save.'%</span></div><span style="color:red;font-size:12px;">'.rupiah($row['harga_promo']).'</span>';



    }else{
      $harga = rupiah($row['harga_normal']);
    }

    echo ' <div class="col-50">

    <div class="card-produk">
    <div style="background-image:url(https://growcery.id/foto/produk/'.$row['gambar'].');    height: 162px;
    background-size: cover;
    background-position: center;" class="card-header align-items-flex-end produk-'.$row['id'].'">
    <div class="status_produk">'.$row['status'].'</div>
    <div class="cardbeli-'.$row['id'].' atur-bg" style="display:none;">
    <p class="text_cart"><span class="totalcart-'.$row['id'].'">1</span><br> Item</p>
    </div>
    </div>
    <div class="card-content card-content-padding" style="padding:8px;">
    <p class="p-title" style="color:#000;min-height:34px;max-height:34px;"><b>'.$row['nama_produk'].'</b></p>
    <p class="p-desc" style="margin-bottom: 10px;min-height:29px;">'.substr($row['keterangan'], 0, 30).'</p>
    <p style="font-size: 12px;"><b>'.$harga.'</b></p>

    <button type="button" class="button button-fill button-small btn-add-temp btn-add-temp-'.$row['id'].'" key="'.$row['id'].'" style="width:100%;">Beli</button>
    <div class="row btnqty-'.$row['id'].'" style="display:none;">
    <button type="button" class="col button button-fill button-small color-red btn-minus" key="'.$row['id'].'">-</button>
    <button type="button" class="col button button-fill button-small btn-plus" key="'.$row['id'].'">+</button>
    </div>
    </div>
    </div>

    </div>';
  }


  

}

public function get_kategori(){
  $get = $this->db->query('SELECT COUNT(*) as total , a.id_kategori , b.nama_kategori FROM produk a LEFT JOIN kategori b ON a.id_kategori = b.id WHERE a.flag=1 GROUP BY a.id_kategori ORDER BY urut ASC')->result_array();

  foreach ($get as $row) {
    $this->produk_kategori($row['id_kategori'],$row['nama_kategori']);
  }
}

public function get_provinsi(){
  $get = $this->db->get_where('provinsi', array('flag' => 1))->result_array();
   echo "<option value=''>-- Pilih Provinsi --</option>";
  foreach ($get as $row) {
    echo '<option value="'.$row['id_prov'].'">'.$row['nama'].'</option>';
  }
}

public function get_kota(){
  $id_prov = $this->input->post('id_provinsi');
  $get = $this->db->get_where('kabupaten', array('id_prov' => $id_prov, 'flag' => 1))->result_array();
  echo "<option value=''>-- Pilih Kota --</option>";
  foreach ($get as $row) {
    echo '<option value="'.$row['id_kab'].'">'.$row['nama'].'</option>';
  }
}

public function get_kecamatan(){
 $id_kota = $this->input->post('id_kota');
 $get = $this->db->get_where('kecamatan', array('id_kab' => $id_kota))->result_array();
echo "<option value=''>-- Pilih Kecamatan --</option>";
 foreach ($get as $row) {
  echo '<option value="'.$row['id_kec'].'">'.$row['nama'].'</option>';
}
}

public function get_kelurahan(){
 $id_kec = $this->input->post('id_kec');
 $get = $this->db->get_where('kelurahan', array('id_kec' => $id_kec))->result_array();
echo "<option value=''>-- Pilih Kelurahan --</option>";
 foreach ($get as $row) {
  echo '<option value="'.$row['id_kel'].'">'.$row['nama'].'</option>';
}
}

public function simpan_alamat(){
  if($post = $this->input->post()){
    $data['nama'] = $post['nama'];
    $data['telephone'] = $post['no_hp'];
    $data['id_provinsi'] = $post['id_provinsi'];
    $data['id_kota'] = $post['id_kota'];
    $data['id_kecamatan'] = $post['id_kecamatan'];
    $data['id_kelurahan'] = $post['id_kelurahan'];
    $data['kode_pos'] = $post['kode_pos'];
    $data['alamat'] = $post['alamat'];
    $data['id_user'] = $post['id_user'];

    if($this->db->insert('alamat_pengiriman',$data)){
      $json['success'] = true;
      $json['message'] = 'Alamat berhasil di simpan';

      echo json_encode($json);
    }
  }
}


public function cek_pengiriman(){
  $id_transaksi = $this->input->post('id_transaksi');
  $get = $this->db->get_where('transaksi_temp', array('id' => $id_transaksi))->row_array();
  if($get['id_pengiriman'] == 0){
    $json['success'] = false;
    $json['message'] = 'Alamat pengiriman harus diisi';
   
  }else{
   
    $upp['id_cabang'] = $this->input->post('id_cabang');
    $upp['tgl_pengiriman'] = $this->input->post('tgl_pengiriman');
    if($this->db->update('transaksi_temp',$upp , array('id' => $id_transaksi))){
      $json['success'] = true;
      $json['message'] = 'Berhasil simpan alamat';

    }
    
  }
  echo json_encode($json);

  
}

}