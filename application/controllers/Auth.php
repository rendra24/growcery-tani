<?php

header('Access-Control-Allow-Origin: *');  

class Auth extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('global_helper');
        //$this->load->library('google');
        //load model admin
    }


    public function index(){

        //$data['loginURL'] = $this->google->createAuthUrl();
        $this->load->view('web/login');
    }

    // public function index_google(){

    //     if(isset($_GET['code'])){
    //         // Otentikasi pengguna dengan google
    //         $client = $this->google;
    //         $client->authenticate($_GET['code']);
    //         # ambil profilenya
    //         $gp = new Google_Service_Plus($client);
    //         $profil = $gp->people->get("me"); 
    //         // data untuk di input ke database
    //         $dataPengguna['oauth_provider'] = 'google';
    //         $dataPengguna['oauth_uid'] = $profil['id'];
    //         $dataPengguna['nama_depan'] = $profil['name']['familyName'];
    //         $dataPengguna['nama_belakang'] = $profil['name']['givenName'];
    //         $dataPengguna['email'] = $profil['emails']['0']['value'];
    //         $dataPengguna['jenis_kelamin'] = !empty($profil['gender'])?$profil['gender']:'';
    //         $dataPengguna['photo'] = !empty($profil['image']['url'])?$profil['image']['url']:'';
    //         // Insert atau update data pengguna di database
    //         $userID = $this->pengguna->checkUser($dataPengguna);
    //         // simpan session
    //         $this->session->set_userdata('telahLogin', true);
    //         $this->session->set_userdata('dataPengguna', $dataPengguna);
    //         // Redirect to profile page
    //         redirect('Welcome/profil/');
    //     } 
    //     // Google authentication url
    //     $data['loginURL'] = $this->google->createAuthUrl();
    //     // Load google login view
    //     $this->load->view('login',$data);
    // }

    public function daftar(){
        $this->load->view('web/daftar');
    }

    public function do_login(){

        if($post = $this->input->post()){

            $email = $post['email'];
            $userpass = $post['password'];



            $cek = $this->db->get_where('users',  array('email' => $email));

            if($cek->num_rows() > 0){
                $row = $cek->row_array();

                if (password_verify($userpass, $row['password'])) {
                 $newdata = array(
                    'id_user'   => $row['id'],
                    'nama'      => $row['nama'],
                    'email'     => $row['email'],
                    'logged_in' => TRUE
                );

                 $this->session->set_userdata($newdata);
                 echo 1;
             }

         }else{
            echo "password atau email salah";
        }
    }else{
        echo "Error Server";
    }
}

public function login_google(){

    if($post = $this->input->post()){

        $cek = $this->db->get_where('users', array('email' => $post['email']));

        if($cek->num_rows() > 0){
            $add['login_google'] = 1;
            $add['nama'] = $post['nama'];

            if($post['emailVerified'] == true){
                $add['emailVerified'] = 1;
            }else{
                $add['emailVerified'] = 0;
            }

            if($this->db->update('users', $add, array('email' => $post['email']))){
                $row = $cek->row_array();

                $newdata = array(
                    'id_user'   => $row['id'],
                    'nama'      => $row['nama'],
                    'email'     => $row['email'],
                    'logged_in' => TRUE
                );

                 $this->session->set_userdata($newdata);
                echo 1;
            }
        }else{
             $add['login_google'] = 1;
            $add['nama'] = $post['nama'];
            $add['email'] = $post['email'];

            if($post['emailVerified'] == true){
                $add['emailVerified'] = 1;
            }else{
                $add['emailVerified'] = 0;
            }

            if($this->db->insert('users', $add)){
                 $newdata = array(
                    'id_user'   => $this->db->insert_id(),
                    'nama'      => $post['nama'],
                    'email'     => $post['email'],
                    'logged_in' => TRUE
                );

                 $this->session->set_userdata($newdata);
                echo 1;
            }
        }
    }
}

public function do_regis(){
    if($post = $this->input->post()){

        $cek = $this->db->get_where('users', array('email' => $post['email']));

        if($cek->num_rows() > 0){
            echo "Email sudah di gunakan";
        }else{
            $data['email'] = $post['email'];
            $data['nama'] = $post['nama'];
            $data['telephone'] = $post['hp'];
            $data['password'] = password_hash($post['password'], PASSWORD_BCRYPT);


            if($this->db->insert('users', $data)){
                echo 1;
            } 
        }


    }
}


public function keluar(){
     // $this->session->unset_userdata('id_user');
     //    $this->session->unset_userdata('nama');
     //    $this->session->unset_userdata('email');
     //     $this->session->unset_userdata('logged_in');

         session_destroy();

         redirect(base_url());
}




}