<?php 
$id_transaksi = $this->input->post('id_transaksi');
$status = $this->input->post('status');

if($status == 'selesai'){
        $transaksi = $this->db->query("SELECT a.*, a.tgl_kirim as tgl_pengiriman ,  b.nama, b.alamat, b.nama , c.nama as provinsi, d.nama as kota, e.nama as kecamatan  FROM transaksi a 
                                    LEFT JOIN alamat_pengiriman b ON a.id_pengiriman = b.id
                                    LEFT JOIN provinsi c ON b.id_provinsi = c.id_prov
                                    LEFT JOIN kabupaten d ON b.id_kota = d.id_kab
                                    LEFT JOIN kecamatan e ON b.id_kecamatan = e.id_kec
                                    WHERE a.id = '".$id_transaksi."' ")->row_array();

        $detail = $this->db->query("SELECT a.*, b.nama_produk FROM transaksi_detail a 
                                    LEFT JOIN produk b ON a.id_produk = b.id
                                    WHERE id_transaksi = '".$id_transaksi."' ")->result_array();

        $status_now = '1';
}else{
  $transaksi = $this->db->query("SELECT a.*, b.nama, b.alamat , b.nama ,  c.nama as provinsi, d.nama as kota, e.nama as kecamatan  FROM transaksi_temp a 
                                    LEFT JOIN alamat_pengiriman b ON a.id_pengiriman = b.id
                                    LEFT JOIN provinsi c ON b.id_provinsi = c.id_prov
                                    LEFT JOIN kabupaten d ON b.id_kota = d.id_kab
                                    LEFT JOIN kecamatan e ON b.id_kecamatan = e.id_kec
                                    WHERE a.id = '".$id_transaksi."' ")->row_array();

    $detail = $this->db->query("SELECT a.*, b.nama_produk FROM transaksi_temp_detail a 
                                    LEFT JOIN produk b ON a.id_produk = b.id
                                    WHERE id_transaksi = '".$id_transaksi."' ")->result_array();

    $status_now = '0';
}
      

    
 ?>

<div class="d-flex lists">
       	<div><p class="text-comp mb0">Tanggal Pesanan</p></div>
       	<div class="ml-auto"><p class="text-comp mb0"><?php echo $transaksi['tgl_order']; ?></p></div>
       </div>
       <div class="d-flex lists">
       	<div><p class="text-comp mb0">Tanggal Pengiriman</p></div>
       	<div class="ml-auto"><p class="text-comp mb0"><?php if(isset($transaksi['tgl_pengiriman'])){ echo date_indo($transaksi['tgl_pengiriman']); }else{ echo '-'; } ?></p></div>
       </div>
       <!-- <div class="d-flex lists">
       	<div><p class="text-comp mb0">Tanggal Diterima</p></div>
       	<div class="ml-auto"><p class="text-comp mb0">-</p></div>
       </div>
       <div class="d-flex lists">
       	<div><p class="text-comp mb0">Penerima</p></div>
       	<div class="ml-auto"><p class="text-comp mb0">-</p></div>
       </div> -->

       <p style="font-size: 14px;font-weight: 700;margin-top: 15px;">Alamat Pemesanan</p>
       <div class="card" style="margin-bottom: 15px;">
       		<div class="card-body">
       			<p class="text-comp mb0"><b><?php echo $transaksi['nama']; ?></b></p>
       			<p class="text-comp mb0"><?php echo $transaksi['alamat'].' , '.$transaksi['kecamatan'].' , '.$transaksi['kota'].' , '.$transaksi['provinsi']; ?></p>
       		</div>
       </div>

       <p style="font-size: 14px;font-weight: 700;">Daftar Barang</p>
         <?php 
            foreach ($detail as $row) {
      ?>

       <div class="d-flex lists">
       	<div>
       		<p class="text-comp mb0"><?php echo $row['nama_produk']; ?></p>
       		<p class="text-comp mb0 is-grey"><?php echo rupiah($row['harga']); ?> x <?php echo $row['qty']; ?></p>
       	</div>
       	<div class="ml-auto"><p class="text-comp mb0"><b><?php echo rupiah($row['sub_total']); ?></b></p></div>
       </div>

        <?php
            }
          ?>

       <p style="font-size: 14px;font-weight: 700;margin-top: 15px;">Ringkasan</p>
        <div class="d-flex lists">
       	<div><p class="text-comp mb0">Total Belanja</p></div>
       	<div class="ml-auto"><p class="text-comp mb0"><?php echo rupiah($transaksi['grand_total']); ?></p></div>
       </div>

        <div class="d-flex lists">
       	<div><p class="text-comp mb0">Ongkir</p></div>
       	<div class="ml-auto"><p class="text-comp mb0"><?php echo rupiah($transaksi['ongkir']); ?></p></div>
       </div>

        <div class="d-flex lists">
       	<div><p class="text-comp mb0"><b>Total</b></p></div>
       	<div class="ml-auto"><p class="text-comp mb0"><b><?php echo rupiah($transaksi['grand_total'] + $transaksi['ongkir']); ?></b></p></div>
       </div>

       <button type="button" id="batal_tr" class="btn btn-outline-danger btn-block" onclick="batal_pesanan(<?php echo $id_transaksi; ?>,<?php echo $status_now; ?>);">Batalkan Pesanan</button>

