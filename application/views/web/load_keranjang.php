<?php 
						$id_transaksi = $this->session->userdata('kode_transaksi');

						$query_p = "SELECT a.*, b.nama_produk, d.nama_satuan from transaksi_temp_detail as a 
						LEFT JOIN produk as b ON a.id_produk = b.id  
						LEFT JOIN transaksi_temp c ON a.id_transaksi = c.id
						LEFT JOIN satuan d ON b.id_satuan = d.id
						WHERE c.id='$id_transaksi'
						GROUP BY a.id_produk";
						$shop = $this->db->query($query_p);
						
						if($shop->num_rows() > 0){
						$shop = $shop->result_array(); 


						foreach ($shop as $row) {
						?>

						<div class="col-md-12" style="margin-bottom: 15px;">
							<div class="card">
								<div class="card-body" style="padding: 12px;padding-bottom: 0px;">

									<div class="row d-flex" style="margin: 0px;margin-bottom: 20px;">
										<div>
											<p style="font-size: 12px;font-weight: 500;margin-bottom: 0px;"><?php echo $row['nama_produk']; ?></p>
											<p style="color: #9295a6;font-size: 12px;margin-bottom: 0px;"><?php echo rupiah($row['harga']); ?> / <?php echo $row['nama_satuan']; ?></p>		
										</div>
										<div class="ml-auto">
											<button type="button" class="btn btn-default btn-sm delete-keranjang" onclick="delete_keranjang(<?php echo $row['id']; ?>,<?php echo $row['id_produk']; ?>);"><i class="fa fa-trash-o" aria-hidden="true"></i></button>								
										</div>
									</div>

									<input type="hidden" name="id_produk[]" value="<?php echo $row['id_produk'] ?>">
									<input type="hidden" name="harga[]" value="<?php echo $row['harga'] ?>">

									<div class="row d-flex" style="margin: 0px;margin-bottom: 15px;">
										<div>
											<div class="input-group">
											  <input type="button" value="-" class="button-minus btn btn-sm btn-danger" onclick="decrementValue(event,<?php echo $row['id_produk']; ?>);" key="<?php echo $row['id_produk']; ?>" data-field="quantity">
											  <input type="text" step="1" max="" min="1" value="<?php echo $row['qty']; ?>" name="quantity[]" class="quantity-field form-control input-sm" >
											  <input type="button" value="+" class="button-plus btn btn-sm btn-success" onclick="incrementValue(event,<?php echo $row['id_produk']; ?>);" key="<?php echo $row['id_produk']; ?>" data-field="quantity">
											</div>

										</div>
										<div class="ml-auto">
											<p style="font-size: 12px;font-weight: 500;margin-bottom: 0px;" class="hitung-price" key="<?php echo $row['sub_total']; ?>"><?php echo rupiah($row['sub_total']); ?></p>
										</div>
									</div>
									
								</div>
							</div>
						</div>

					<?php }
				}else{
					 ?>

					 	<div style="padding: 50px 25px;text-align: center;">
								
							
							<img src="<?php echo base_url().'assets/empty.svg' ?>" style="margin-bottom: 10px;height: 30px;">
									<p style="font-weight: 700;font-size: 12px;margin-bottom: 0px;">Belanja dan buat pesanan sekarang!</p>
									<p style="font-size: 12px;margin-bottom: 10px;">Temukan produk dengan harga yang kompetitif dan membayar harga yang adil bagi para petani.</p>

									</div>
					 <?php } ?>