
<style type="text/css">
	.card-products{
		box-shadow: 0 0.5em 1em -0.125em rgba(10,10,10,.1), 0 0 0 1px rgba(10,10,10,.02);
		min-height: 350px;
	}
	.quantity-field{
		width: 50px !important;
    padding: 3px 3px !important;
    text-align: center !important;
    margin-right: 10px;
    margin-left: 10px;
    border-radius: 7px !important;
	}

	.button-minus{
		border-radius: 20px;
		width: 30px;
		height: 30px;
	}

	.button-plus{
		border-radius: 20px;
		width: 30px;
		height: 30px;
	}

	.card-products-dekstop{
		box-shadow: 0 6px 8px rgba(184,187,204,.2);
	}

	.swal-button {
  padding: 7px 19px;
  border-radius: 2px;
  background-color: #ffde17;
  font-size: 12px;
  border: 1px solid #ffde17;
  text-shadow: 0px -1px 0px #ffde1761;
}

.swal-button--danger:active{
	background-color: #ffde17 !important;
}

.swal-button--danger:hover{
	background-color: #ffde17 !important;
}

.swal-button--cancel {
    color: #fff;
    background-color: #b7b7b7 !important;
    border: none !important;
}

.swal-button--cancel:focus {
    box-shadow: none !important;
}

#load_keranjang{
	background-color: #f3f4fa;
    padding: 8px;
    max-height: 540px;
    overflow: auto;
}



	@media only screen and (max-width: 600px) {
	 .card-mobile{
	 	display: none;
	 }
	 .card-products-dekstop{
	 	background: transparent;
	 	padding: 0px;
	 	border: none;
	 	box-shadow: none;
	 }
	 .col-mobile{
	 	padding-left: 5px;
	 	padding-right: 5px;
	 }
	 .card-img-top > img {
		height: 165px;
	}
	.container-fluid{
		margin-bottom: 50px;
	}

	}



.badge-promo{
	background-color: orange;
    color: #FFF;
    position: absolute;
    top: 25px;
    font-size: 12px;
    padding: 8px;
    border-top-right-radius: 5px;
    border-bottom-right-radius: 5px;
}

.bagde-hemat{
	background-color: orange;
	color: #FFF;
	padding: 5px;
	font-size: 10px;
	border-radius: 5px;
	margin-left: 10px;
	margin-bottom: 10px;
}


.slick-prev {
    left: 1px !important;
}

.slick-next {
    right: 0px !important;
}

.card-img-top > img {
	height: 215px;
}
</style>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/slick/slick.css' ?>"/>
			<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/slick/slick-theme.css' ?>"/>


<div class="container-fluid jarak-header">

	<div class="row" style="margin-bottom: 20px;margin-top: 20px;">
				<div class="col-md-12" style="margin:0 auto;">
				
					<?php 
					$slide = $this->db->get_where('slider', array('flag' => '1'))->result_array();

					 ?>
							<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
							  <ol class="carousel-indicators">
							  	 <?php 
							  	 	$no = 0;
							  	 	$slider = 0;
							  	 foreach ($slide as $row) { ?>
							    <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $no; ?>" <?php if($no == 0){ echo 'class="active"'; } ?>> </li>
							    <?php
							    $no++;
							     } ?>
							  </ol>
							 
							  <div class="carousel-inner">
							  	 <?php foreach ($slide as $row) { ?>
							    <div class="carousel-item <?php if($slider == 0){ echo 'active'; } ?>">
							      

							      <div style="background-image: url('<?php echo 'https://growcery.id/foto/slider/'.$row['gambar'] ?>'); width: 100%;height: auto;max-height: 637px;background-position: center;background-repeat: no-repeat;min-height: 647px;background-size: cover;">
							      	
							      </div>
							    </div>
							<?php 
							$slider++;
						} ?>
							  </div>
							  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
							    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
							    <span class="sr-only">Previous</span>
							  </a>
							  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
							    <span class="carousel-control-next-icon" aria-hidden="true"></span>
							    <span class="sr-only">Next</span>
							  </a>
							</div>

							




				</div>
			</div>


	<div class="row" style="margin-top: 30px;">
		<div class="col-md-8">


	

			<p style="font-size: 14px;"><b>Kategori</b></p>
			<div class="row" style="margin-bottom: 20px;">
				<div class="col-md-12">
					<div class="responsive">
						<div class="item-slik">
							<a href="<?php echo base_url(); ?>" style="text-decoration: none;color: #000;">
								<div class="card">
									<img class="card-img-top img-fluid" style="background-color: #b8bbcc;" src="https://tanihub.com/static/icons/all-products.svg" alt="">
									<div class="card-body p5 text-center" style="min-height: 39px;">
										<p class="text-slik">Semua Produk</p>
									</div>
								</div>	
							</a>
							</div>

						<?php 
						$kategori = $this->db->get('kategori')->result_array();
						foreach ($kategori as $row) {
							?>


							<div class="item-slik">
								<a href="<?php echo base_url().'?k='.$row['id']; ?>" style="text-decoration: none;color: #000;">
								<div class="card">
									<img class="card-img-top img-fluid" src="<?php echo "https://growcery.id/foto/produk/".$row['gambar']; ?>" alt="<?php echo $row['nama_kategori']; ?>">
									<div class="card-body p5 text-center" style="min-height: 39px;">
										<p class="text-slik"><?php echo $row['nama_kategori']; ?></p>
									</div>
								</div>
								</a>	
							</div>

							<?php
						}
						?>




					</div>
				</div>
			</div>	





			<div class="row">
				<div class="col-md-7">
					
					<div class="input-group mb-3">
						<div class="input-group-prepend">
							<span class="input-group-text" id="basic-addon1" style="background-color: #FFF;border-right: none;"><i class="fa fa-search" aria-hidden="true"></i></span>
						</div>
						<input type="text" class="form-control input-sm" placeholder="Cari buah sayur beras" name="q" style="border-left: none;">
						<div class="input-group-append">
							<button class="btn btn-success btn-sm" type="submit">Cari</button>
						</div>
					</div>
					
				</div>
				<div class="col-md-5">
					<select class="form-control input-sm">
						<option>Paling Sesuai</option>
					</select>
				</div>
			</div>


			<div class="row">
				<div class="col-md-12">
					

					<div class="card card-products-dekstop">
						<div class="card-body" style="padding: 10px;">
							<div class="row">

							<?php 
							$sql_plus = '';

							if(isset($_GET['k'])){
								$sql_plus = " AND a.id_kategori=".$_GET['k'];
							}
							
							$query = "SELECT a.*,  b.nama_satuan from produk as a LEFT JOIN satuan b ON a.id_satuan = b.id WHERE a.flag=1".$sql_plus;
							$produk = $this->db->query($query);

							if($produk->num_rows() > 0){
								echo $this->db->last_query();
								$produk->result_array();

								$harga=0;
							foreach ($produk as $row) {

								if($row['promo'] == 1){
									$save = (($row['harga_normal'] - $row['harga_promo']) / $row['harga_normal']) * 100;

									$save = floor($save);

									$harga = '<div style="font-size:12px;"><del>'. rupiah($row['harga_normal']).'</del><span class="bagde-hemat">Save '.$save.'%</span></div><span style="color:red;font-size:12px;">'.rupiah($row['harga_promo']).'</span>';
	
								}else{
									$harga = rupiah($row['harga_normal']);
								}

								?>
								<div class="col-md-3 col-6 col-mobile" style="margin-bottom: 15px;">
									<div class="card card-products" style="border-radius: 8px;">
										
										 <a class="card-img-top" href="#" style="background-image: url(<?php echo 'https://growcery.id/foto/produk/'.$row['gambar']; ?>);" > 
										 	<img src="<?php echo 'https://growcery.id/foto/produk/'.$row['gambar']; ?>" alt="Eli DeFaria" />
										 	<?php if($row['promo'] == 1){ ?>
										 	<div class="badge-promo">
										 		Promo
										 	</div>
										 <?php } ?>
										 </a>

										<div class="card-body" style="padding: 15px;">
											<div style="min-height: 64px;margin-bottom: 10px;">
											<p class="card-text" style="font-size: 12px;font-weight: 700;margin-bottom: 0px;"><?php echo $row['nama_produk']; ?></p>
											<p style="font-size: 10px;font-weight: 500;"><?php echo substr($row['keterangan'], 0, 30); ?></p>
											</div>

											<div>
												<div style="font-size: 12px !important;margin-bottom: 0px;min-height: 35px;"><b><?php echo $harga; ?> </b> <span style="font-size: 12px;">/ 1 <?php echo $row['nama_satuan']; ?></span></div>
												<button class="btn btn-sm btn-success btn-block btn-add-temp" key="<?php echo $row['id']; ?>">Tambah</button>
											</div>
										</div>
									</div>
								</div>
							<?php 
								}
							}else{
							 ?>

							 	<div style="margin:0 auto;padding: 50px 25px;text-align: center;">
								
							
							<img src="<?php echo base_url().'assets/empty.svg' ?>" style="margin-bottom: 10px;height: 30px;">
									<p style="font-weight: 700;font-size: 12px;margin-bottom: 0px;">Produk Kosong!</p>

									</div>
							<?php } ?>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-4">
			<div class="card card-mobile">
				<div class="card-body">
					<p style="font-size: 14px;font-weight: 700;">Keranjang Belanja</p>
					<div class="loader-wrapper">
							    <div class="loader-animation">
							      <svg><path d="M442 79.1H0V65.5h412.4v-7.1H0V0h442v79.1zm0 7.1V107H181.2v-7.1H0V86.2h442zM50.1 24.6v7.2h53.3v-7.2H50.1zm0-16.8v7.1h89.3V7.8H50.1zM19.3 38.9c10.6 0 19.2-8.7 19.2-19.4C38.5 8.7 30 0 19.3 0A19.4 19.4 0 0 0 0 19.5c0 10.7 8.6 19.4 19.3 19.4z"/></svg>
							    </div>
							  </div>

							  <form action="<?php echo base_url().'web/add_checkout' ?>" method="post" id="form_add_temp">
							  	
							  
					<div class="row" id="load_keranjang" style="display: none;">
						
						<?php 
						$id_user = $this->session->userdata('id_user');
						


						$query_p = "SELECT a.*, b.nama_produk from transaksi_temp_detail as a 
									LEFT JOIN produk as b ON a.id_produk = b.id 
									LEFT JOIN transaksi_temp c ON a.genrate_id = c.genrate_id
									WHERE a.id_user='$id_user' AND c.status != '2' GROUP BY a.id_produk";
						$shop = $this->db->query($query_p);

						if($shop->num_rows() > 0){
							$shop = $shop->result_array(); 
							$grand_total = 0;

						foreach ($shop as $row) {
							$grand_total += $row['sub_total'];
							?>

							<div class="col-md-12" style="margin-bottom: 15px;">
							<div class="card">
								<div class="card-body" style="padding: 12px;padding-bottom: 0px;">

									<div class="row d-flex" style="margin: 0px;margin-bottom: 20px;">
										<div>
											<p style="font-size: 12px;font-weight: 500;margin-bottom: 0px;"><?php echo $row['nama_produk']; ?></p>
											<p style="color: #9295a6;font-size: 12px;margin-bottom: 0px;"><?php echo rupiah($row['harga']); ?> / Karton</p>		
										</div>
										<div class="ml-auto">
											<button type="button" class="btn btn-default btn-sm delete-keranjang" 
											onclick="delete_keranjang(<?php echo $row['id']; ?>);"><i class="fa fa-trash-o" aria-hidden="true"></i></button>								
										</div>
									</div>

									<input type="hidden" name="id_produk[]" value="<?php echo $row['id_produk'] ?>">
									<input type="hidden" name="harga[]" value="<?php echo $row['harga'] ?>">

									<div class="row d-flex" style="margin: 0px;margin-bottom: 15px;">
										<div>
											<div class="input-group">
											  <button type="button" class="button-minus btn btn-sm btn-danger" data-field="quantity">-</button>
											  <input type="text" step="1" max="" min="1" value="<?php echo $row['qty']; ?>" name="quantity[]" class="quantity-field form-control input-sm" >
											  <button type="button" class="button-plus btn btn-sm btn-success" data-field="quantity">+</button>
											</div>

										</div>
										<div class="ml-auto">
											<p style="font-size: 12px;font-weight: 500;margin-bottom: 0px;"><?php echo rupiah($row['sub_total']); ?></p>
										</div>
									</div>
									
								</div>
							</div>
						</div>

							<?php } ?>

						

							<?php
						}else{
							?>
							<div style="padding: 50px 25px;text-align: center;">
								
							
							<img src="<?php echo base_url().'assets/empty.svg' ?>" style="margin-bottom: 10px;height: 30px;">
									<p style="font-weight: 700;font-size: 12px;margin-bottom: 0px;">Belanja dan buat pesanan sekarang!</p>
									<p style="font-size: 12px;margin-bottom: 10px;">Temukan produk dengan harga yang kompetitif dan membayar harga yang adil bagi para petani.</p>

									</div>

							<?php
						}

						
						?>

					</div>

					</form>
						<div class="d-flex" style="margin-top: 15px; padding: 8px;">
						<div>
							<p style="margin-bottom: 0px;" class="hitung-grandtotal">Subtotal: <b><?php echo rupiah(@$grand_total); ?></b></p>
						</div>
						<div class="ml-auto">
							<button type="button" id="btn-checkout" class="btn btn-primary btn-sm <?php if(!isset($grand_total)){ echo "btn-secondary"; } ?>" <?php if(!isset($grand_total)){ echo "disabled"; } ?> >Checkout</a>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>




<script type="text/javascript">
	function rupiah(angka, prefix){
			var number_string = angka.toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}
 
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
		}


function delete_keranjang(id){
	$('.loader-wrapper').css('display', 'block');
  		$('#load_keranjang').css('display', 'none');
		
		$.ajax({
				url : "<?php echo base_url().'web/delete_tr_temp' ?>",
				type: "POST",
				data : {id:id}
			}).done(function(response){
				$('#load_keranjang').html(response);
						var sum = 0;
					    $('.hitung-price').each(function() {
					        sum += Number($(this).attr('key'));
					    });

					    $('.loader-wrapper').css('display', 'none');
  						$('#load_keranjang').css('display', 'block');

					    if(sum == 0){
							$('#btn-checkout').addClass('btn-secondary');
							$('#btn-checkout').removeClass('btn-primary');
							$('#btn-checkout').prop('disabled', true);
					    }else{
					    	$('#btn-checkout').removeClass('btn-secondary');
							$('#btn-checkout').addClass('btn-primary');
							$('#btn-checkout').removeAttr("disabled");
					    }


					$('.hitung-grandtotal').html("Subtotal: <b>"+rupiah(sum, 'Rp ')+"</b>");
			});
}



	$('.btn-add-temp').click(function(){
		var id_user = "<?php echo $this->session->userdata('id_user'); ?>";
		if(id_user == ''){
			swal({
			  title: "Maaf",
			  text: "Anda harus masuk terlebih dahulu !",
			  icon: "warning",
			  buttons: true,
			  dangerMode: true,
			})
			.then((willDelete) => {
			  if (willDelete) {
			    window.location = "<?php echo base_url().'masuk' ?>";
			  }
			});
		}else{
			$(this).removeClass('btn-success');
			$(this).addClass('btn-secondary');
			$(this).prop('disabled', true).text('Loading..');
			$('.loader-wrapper').css('display', 'block');
  			$('#load_keranjang').css('display', 'none');
			var id_produk = $(this).attr('key');
			var qty = 1;

			$.ajax({
					url : "<?php echo base_url().'web/add_tr_temp'; ?>",
					type: "POST",
					data : {id_produk:id_produk, qty:qty}
				}).done(function(response){
					
						$('#load_keranjang').html(response);
						$('.btn-add-temp').removeClass('btn-secondary');
						$('.btn-add-temp').addClass('btn-success');
						$('.btn-add-temp').removeAttr("disabled").text('Tambah');
						$('.loader-wrapper').css('display', 'none');
  						$('#load_keranjang').css('display', 'block');


  						var sum = 0;
					    $('.hitung-price').each(function() {
					        sum += Number($(this).attr('key'));
					    });

					    if(sum == 0){
							$('#btn-checkout').addClass('btn-secondary');
							$('#btn-checkout').removeClass('btn-primary');
							$('#btn-checkout').prop('disabled', true);
					    }else{
					    	$('#btn-checkout').removeClass('btn-secondary');
							$('#btn-checkout').addClass('btn-primary');
							$('#btn-checkout').removeAttr("disabled");
					    }


					$('.hitung-grandtotal').html("Subtotal: <b>"+rupiah(sum, 'Rp ')+"</b>");

					

					
				});
		}
		
	});


	$('#btn-checkout').click(function(){
		$('#form_add_temp').submit();
	});

	$("#form_add_temp").submit(function(event){
			event.preventDefault();
			$('#btn-checkout').removeClass('btn-success');
			$('#btn-checkout').addClass('btn-secondary');
			$('#btn-checkout').prop('disabled', true).text('Loading..');
			var post_url = $(this).attr("action");
			var request_method = $(this).attr("method");
			var form_data = $(this).serialize();

			$.ajax({
				url : post_url,
				type: request_method,
				data : form_data
			}).done(function(response){
				// if(response == 1){
				
					window.location = '<?php echo base_url().'checkout'; ?>';
				// }
			});
		});
</script>
