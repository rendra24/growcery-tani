<div class="container-fluid" style="padding-left: 80px;padding-right: 80px;">
	<div class="row" style="margin-top: 30px;">
		<div class="col-md-3">
			<div class="row">
				<div class="col-md-12" style="padding: 15px;background-color: #FFF;border-radius: 4px;margin-bottom: 20px;">
					<p style="font-size: 16px;font-weight: 700;margin-bottom: 0px;"><i class="fa fa-user" style="font-size: 16px;color: #28a745;">&nbsp;&nbsp;</i> Akun Saya</p>
				</div>
				<div class="col-md-12" style="padding: 15px;background-color: #FFF;border-radius: 4px;margin-bottom: 20px;">
					<p style="font-size: 16px;font-weight: 700;margin-bottom: 0px;"><i class="fa fa-shopping-bag" aria-hidden="true"style="font-size: 16px;color: #28a745;">&nbsp;&nbsp;</i> Pesanan Saya</p>
				</div>
				<div class="col-md-12" style="padding: 15px;background-color: #FFF;border-radius: 4px;">
					<p style="font-size: 16px;font-weight: 700;margin-bottom: 0px;"><i class="fa fa-user" style="font-size: 16px;color: #28a745;">&nbsp;&nbsp;</i> Tagihan Saya</p>
				</div>
			</div>
		</div>

		<div class="col-md-9">
			<div class="card">
				<div class="card-body">
					<p style="font-weight: 700;font-size: 20px;line-height: 25px;margin-bottom: 20px;">Pesanan Saya</p>
					
					<div class="card" style="margin-top: 20px;">
						<div class="card-body" style="text-align: center;padding: 60px 20px;">
							<img src="<?php echo base_url().'assets/empty.svg' ?>" style="margin-bottom: 10px;">
							<p style="font-weight: 700;font-size: 14px;margin-bottom: 0px;">Belanja dan buat pesanan sekarang!</p>
							<p style="font-size: 14px;margin-bottom: 10px;">Dukung petani kita dan hasil pangan lainnya dengan memilih produk lokal dalam keseharianmu bersama kami!</p>
							<a href="" class="btn btn-success btn-sm">Belanja Sekarang</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>