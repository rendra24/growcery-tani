<style>
/* The radionya */
.radionya {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 14px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default radio button */
.radionya input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
}

/* Create a custom radio button */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: #eee;
  border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.radionya:hover input ~ .checkmark {
  background-color: #ccc;
}

/* When the radio button is checked, add a blue background */
.radionya input:checked ~ .checkmark {
  background-color: #2196F3;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the indicator (dot/circle) when checked */
.radionya input:checked ~ .checkmark:after {
  display: block;
}

/* Style the indicator (dot/circle) */
.radionya .checkmark:after {
  top: 9px;
  left: 9px;
  width: 8px;
  height: 8px;
  border-radius: 50%;
  background: white;
}
</style>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" style="margin-top: 150px;">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pilih Area Pengiriman</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      
          
      
          <?php 
          $this->db->group_by("kota");
          $cabang = $this->db->get_where('cabang', array('flag' => 1))->result_array();
            foreach ($cabang as $row) { ?>
               

            <label class="radionya"><?php echo $row['kota']; ?>
              <input  type="radio" name="cabang" class="radio-cabang" value="<?php echo $row['id']; ?>" >
              <span class="checkmark"></span>
            </label>

            <!-- <div class="funkyradio-success">
              <input type="radio" name="cabang" class="radio-cabang" value="<?php echo $row['id']; ?>" />
              <label for="radio1"><?php echo $row['nama']; ?></label>
            </div> -->
             <?php
            }
           ?>
    <!--  <div class="funkyradio">    
    </div> -->
      
      </div>
    </div>
  </div>
</div>

<?php 
  if($id_user = $this->session->userdata('id_user')){

 ?>
<!-- <div class="row footer-mobile">
  <div class="col-4 <?php  if(isset($active)){
      if($active == 'home'){
        echo "foot-active";
      }else{
        echo "foot";
      }
    }else{ echo "foot"; } ?> " style="text-align: center;"> 
    <a href="<?php echo base_url(); ?>">
      <i class="fa fa-home icon-foot"></i>
      <p class="p-foot">Home</p>
    </a>
  </div>


  <div class="col-4 <?php  if(isset($active)){
      if($active == 'history'){
        echo "foot-active";
      }else{
        echo "foot";
      }
    }else{ echo "foot"; } ?>" style="text-align: center;">
    <a href="<?php echo base_url().'history'; ?>">
    <i class="fa fa-shopping-bag icon-foot"></i>
    <p class="p-foot">Pesanan</p>
  </a>
  </div>


  <div class="col-4 <?php  if(isset($active)){
      if($active == 'account'){
        echo "foot-active";
      }else{
        echo "foot";
      }
    }else{ echo "foot"; } ?>" style="text-align: center;">
    <a href="<?php echo base_url().'account'; ?>">
    <i class="fa fa-user icon-foot"></i>
    <p class="p-foot">Akun</p>
  </a>
  </div>
</div> -->
<?php } ?>
<div class="row footer" style="margin:0px;background-color: #FFF;margin-top: 30px;padding-top: 20px;padding-bottom: 20px;">
	
		<div class="col-md-7">
			<div class="row">
				<div class="col-md-4">
					
				</div>
				<div class="col-md-4">
					<p>Info</p>
					<p class="text-footer">Tentang Growcery</p>
					<p class="text-footer">Mitra</p>
					<p class="text-footer">Supllier</p>
					<p class="text-footer">Berita</p>
				</div>
				<div class="col-md-4">
					<p class="text-footer">Butuh bantuan ? Hubungi kami</p>
					<p class="text-footer"><i class="fa fa-envelope">&nbsp;</i> growceryindonesia@gmail.com</p>
					<p class="text-footer"><i class="fa fa-instagram">&nbsp;</i> <a href="https://www.instagram.com/growcery.indonesia/">grocery.indonesia</a></p>
				</div>
			</div>
		</div>

		<div class="col-md-4">
			<div class="row">

				<div class="col--md-8">
				<p class="text-component mb-8 is-bold  is-small">Download Growcery App</p>
			

				<a href="https://play.google.com/store/apps/details?id=com.growcery.id" target="_blank" rel="noopener noreferrer"><img src="https://growcery.id/foto/google-play.svg" alt="Google Play"></a>
				</div>

				<div class="col-md-4">
					<img src="https://growcery.id/foto/5.jpg" alt="Apps Preview" style="width: 271px;height: 244px;">
				</div>
		</div>
		</div>
	
</div>





<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script type="text/javascript">

 
  
$(document).ready(function(){

  $.ajax({
          url : "<?php echo base_url().'web/count_cart'; ?>",
          type: "POST",
          dataType: "json"
          }).done(function(response){
            $('.cart_total').text(response.qty);
            $("#cart_grand_total").text(response.total);
            
            if(response.qty > 0){
              $(".pad-web").css('display','block');
            }else{
              $(".pad-web").css('display','none');
            }
          });

  <?php 
    if(isset($_GET['f'])){
  ?>
  var f = "<?php echo $_GET['f']; ?>";
      $(".select-filter").val(f);
  <?php
    }
   ?>


  $('.card-img-top').addClass('is-loaded');
  $('.loader-wrapper').css('display', 'none');
  $('#load_keranjang').css('display', 'block');

  <?php if(!$this->session->userdata('id_cabang')){ ?>
  $('#exampleModal').modal('show');
<?php } ?>
 
})



$('.cari-form').keypress(function(e){

            var cari = $(this).val();
              if(e.which == 13){
                e.preventDefault();
              var cari = $(this).val();
              window.location.href = "<?php echo base_url().'?q=' ?>"+cari;    

              }
});


$('.radio-cabang').click(function(){
  var id_cabang = $(this).val();

      $.ajax({
        url : "<?php echo base_url().'web/update_sess_cabang'; ?>",
        type: "POST",
        data : {id_cabang:id_cabang}
      }).done(function(response){
        window.location = "<?php echo base_url(); ?>";
      });
});

$('.carosel-slick').slick({
  dots: true,
  infinite: false,
  autoplay: true,
  speed: 200,
  cssEase: 'linear'
});


	$('.responsive').slick({
  dots: false,
  infinite: false,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 4,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
</script><!-- 
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
</body>
</html>