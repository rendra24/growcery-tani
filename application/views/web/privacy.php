<b>Kebijakan Privasi</b><br>
MOHON ANDA MEMBACA KEBIJAKAN PRIVASI INI DENGAN SEKSAMA UNTUK MEMASTIKAN BAHWA ANDA MEMAHAMI BAGAIMANA Growcery MENGUMPULKAN, MENYIMPAN, MENGGUNAKAN, MEMINDAHKAN, MENGUNGKAPKAN DAN MELINDUNGI INFORMASI PRIBADI YANG DIPEROLEH MELALUI APLIKASI KAMI. <br><br>

Penggunaan Anda atas Aplikasi dan Layanan kami tunduk pada Ketentuan Penggunaan dan Kebijakan Privasi ini dan mengindikasikan persetujuan Anda terhadap Ketentuan Penggunaan dan Kebijakan Privasi tersebut. <br><br>

<b>Definisi</b><br><br>
"Aplikasi" berarti aplikasi Growcery yang tersedia di google play store, app store maupun store yang tersedia dalam sistem perangkat Anda.

"Informasi Pribadi" berarti data perseorangan/perusahaan tertentu yang melekat dan dapat diidentifikasi pada suatu individu/perusahaan dan yang dikumpulkan melalui Aplikasi, seperti nama, alamat, nomor identitas (apabila Anda adalah seorang individu), data dan dokumen identitas perusahaan (apabila Anda bukan seorang individu), nomor telepon, alamat surat elektronik (e-mail), nomor rekening bank, perizinan dan/atau sejenisnya, dan informasi lain yang mungkin dapat mengidentifikasi Anda sebagai pengguna Aplikasi.<br><br>

"Growcery" berarti PT Tani Hub Indonesia, suatu perusahaan yang didirikan berdasarkan hukum Negara Republik Indonesia.<br><br>

"Ketentuan Penggunaan" berarti syarat dan ketentuan atau prosedur standar operasi atau ketentuan lainnya sehubungan dengan masing-masing Aplikasi yang dikembangkan oleh Growcery, sebagaimana dapat diubah atau ditambah dari waktu ke waktu;<br><br>

"Layanan" berarti hal-hal yang ditawarkan oleh Growcery melalui Aplikasi kepada Anda, termasuk namun tidak terbatas pada pemrosesan dan pengantaran untuk produk yang disediakan dari waktu ke waktu melalui Aplikasi.<br><br>

<b>Tujuan Informasi Pribadi yang Growcery Kumpulkan</b><br><br>
Growcery mengumpulkan Informasi Pribadi tertentu dari Anda agar Aplikasi dapat menjalankan fungsinya termasuk namun tidak terbatas dengan tujuan untuk menghubungi Anda tentang akun Anda dalam Aplikasi dan Layanan Growcery, memberikan Layanan kepada Anda, memberi Anda iklan dan pemasaran terkait Layanan Growcery, menjawab pertanyaan-pertanyaan dan/atau permintaan-permintaan yang Anda ajukan kepada Growcery, dan untuk mendeteksi, mencegah, mengurangi dan menyelidiki aktivitas curang atau ilegal.<br><br>

<b>Pemberian Informasi Pribadi oleh Anda</b><br><br>
Informasi Pribadi dapat Anda berikan secara langsung (sebagai contoh, saat Anda mendaftar sebagai pengguna Aplikasi) maupun terkumpul ketika Anda menggunakan Aplikasi.<br><br>

<b>Penggunaan Cookie</b><br><br>

Cookie adalah berkas data kecil yang ditempatkan browser Anda pada perangkat Anda. Dengan cookie, Growcery lebih mengetahui pola penggunaan Anda dan mengingat informasi yang akan membuat penggunaan Anda atas Aplikasi menjadi lebih mudah dan nyaman (misalnya dengan mengingat pengaturan preferensi).<br><br>

<b>Penggunaan Informasi Pribadi Yang Growcery Kumpulkan</b><br><br>

Growcery menggunakan Informasi Pribadi untuk tujuan verifikasi kepemilikan Anda atas suatu akun dalam Aplikasi kami, untuk berkomunikasi dengan Anda sehubungan dengan pemrosesan, penggunaan atau pengelolaan Layanan dan Aplikasi dan untuk memberikan Anda informasi mengenai Aplikasi dan Layanan. Kami juga dapat menggunakan Informasi Pribadi untuk mengirimkan pesan, pembaharuan yang bersifat umum atas Aplikasi dan Layanan, penawaran-penawaran khusus atau promosi-promosi.<br><br>

Growcery menggunakan Informasi Pribadi secara keseluruhan untuk menganalisa pola penggunaan Anda atas Aplikasi. Anda dengan ini setuju bahwa Informasi Pribadi Anda akan digunakan oleh sistem pemrosesan data internal Growcery untuk memastikan diberikannya Layanan yang terbaik dalam Aplikasi untuk Anda.<br><br>

<b>Pemberian Informasi yang Growcery Kumpulkan</b><br><br>

Anda setuju bahwa Growcery dapat memberikan Informasi Pribadi Anda kepada pihak-pihak ketiga baik perusahaan maupun perorangan dimana Growcery bekerjasama untuk memfasilitasi atau memberikan bantuan operasional atau pengembangan Aplikasi dan layanan-layanan tertentu untuk dan/atau atas nama Growcery, untuk, di antaranya, (i) memberikan pelayanan konsumen; (ii) melaksanakan layanan-layanan terkait dengan Aplikasi (termasuk namun tidak terbatas pada layanan pemeliharaan, pengelolaan database, analisis penggunaan Aplikasi dan penyempurnaan fitur-fitur dalam Aplikasi); (iii) membantu Kami dalam menganalisa bagaimana Aplikasi dan Layanan digunakan serta bagaimana pengembangannya; atau (iv) untuk membantu penasihat profesional dan auditor eksternal Kami, termasuk penasihat hukum, penasihat keuangan, dan konsultan-konsultan terkait lainnya. Para pihak ketiga ini hanya memiliki akses atas Informasi Pribadi Anda untuk melakukan tugas-tugas tersebut untuk dan/atau atas nama Kami dan tidak mengungkapkan atau menggunakan Informasi Pribadi tersebut untuk tujuan lain apapun.<br><br>

Growcery tidak membagikan Informasi Pribadi Anda kepada pihak manapun yang berkepentingan selain serta pihak ketiga dengan tujuan penggunaan yang spesifik yang disebutkan di paragraf pertama bagian ini, tanpa persetujuan dari Anda. Namun demikian, Kami akan mengungkapkan Informasi Pribadi Anda sepanjang diwajibkan secara hukum, atau diperlukan untuk tunduk pada ketentuan peraturan perundang-undangan, institusi pemerintah, atau dalam hal terjadi sengketa, atau segala bentuk proses hukum antara Anda dan Growcery, atau antara Anda dan pihak lain sehubungan dengan, atau terkait dengan Aplikasi dan Layanan, atau dalam keadaan darurat yang berkaitan dengan keamanan Anda.<br><br>

<b>Perlindungan Data dan Informasi Pribadi</b><br><br>
Perlindungan data dan Informasi Pribadi Anda adalah suatu kewajiban bagi Growcery. Growcery akan memberlakukan langkah-langkah untuk melindungi dan mengamankan data dan Informasi Pribadi Anda. Namun demikian, Growcery tidak dapat sepenuhnya menjamin bahwa sistem Growcery tidak akan diakses soleh virus, malware, gangguan atau kejadian luar biasa termasuk akses oleh pihak ketiga yang tidak berwenang. Anda harus menjaga keamanan dan kerahasiaan data yang berkaitan dengan akun Anda pada Aplikasi termasuk kata sandi dan data-data lainnya yang Anda berikan di dalam Aplikasi ini.<br><br>

<b>Perubahan atas Kebijakan Privasi ini </b><br><br>
Growcery dapat mengubah Kebijakan Privasi ini untuk sejalan dengan perkembangan kegiatan bisnis Growcery atau dipersyaratkan oleh peraturan perundang-undangan dan instusi pemerintah terkait. Jika Growcery mengubah Kebijakan Privasi ini, Growcery akan memberitahu Anda melalui surat elektronik (e-mail) atau memberikan notifikasi pada Aplikasi 1 (satu) hari sebelum perubahan berlaku. Growcery meminta Anda untuk meninjau Aplikasi secara reguler dan terus-menerus selama Anda menggunakan Aplikasi untuk mengetahui informasi terbaru tentang bagaimana ketentuan Kebijakan Privasi ini Growcery diberlakukan.<br><br>

<b>Lain-lain </b><br><br>
Hukum yang Mengatur dan Yurisdiksi <br><br>

Kebijakan Privasi ini diatur oleh dan ditafsirkan menurut hukum Negara Republik Indonesia. Setiap dan seluruh sengketa yang timbul dari Kebijakan Privasi ini akan diatur oleh yurisdiksi eksklusif dari Pengadilan Negeri Jakarta Selatan.<br><br>

Pengakuan dan Persetujuan
Dengan menggunakan Aplikasi, Anda mengakui bahwa Anda telah membaca dan memahami Kebijakan Privasi ini dan Ketentuan Penggunaan dan setuju dan sepakat terhadap penyimpanan, penggunaan, pemrosesan, pemberian dan pengalihan Informasi Pribadi Anda oleh Growcery sebagaimana dinyatakan di dalam Kebijakan Privasi ini.<br><br>

Growcery juga menyatakan bahwa Anda memiliki hak untuk memberikan seluruh informasi yang telah Anda berikan kepada Growcery dan untuk memberikan hak kepada Growcery untuk memproses dan menggunakan serta memberikan informasi guna mendukung dan melaksanakan fungsi Aplikasi dan membebaskan Growcery dari segala tuntutan apabila di kemudian hari terdapat tuntutan ketidakabsahan hak Anda dalam memberikan informasi.<br><br>

Cara untuk Menghubungi Growcery
Jika Anda memiliki pertanyaan lebih lanjut tentang privasi dan keamanan informasi Anda atau ingin memperbarui atau menghapus data Anda maka silakan hubungi kami di: help@Growcery.com<br><br>